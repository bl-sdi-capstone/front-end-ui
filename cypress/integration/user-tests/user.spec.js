// let gUserID = "";
let gUserID = "qfr8l";

const fName01 = "Cypress";
const lName01 = "TestCase";
const emaiEndl01 = ".cypress@case.test";
let email01 = gUserID + emaiEndl01;
const pNumber01 = "5551119999";
const city01 = "Somewhere";
const state01 = "HI";
const hub01 = "Remote";

const efName01 = "sserpyC";
const elName01 = "esaCtseT";
let eemail01 = email01 + "e";
const epNumber01 = "5559991111";
const ecity01 = "SomewhereElse";
const estate01 = "OH";
const ehub01 = "Bloomington";

function generateRandomAlphaNumericUserID() {
    let text = "";
    const possible = "abcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    gUserID = text;
    return text;
}

function fieldValidationCheck(rURL, fieldElement, pageButton, whatToType, errorMessageUsername) {
    if (rURL != '')
        cy.visit(rURL);

    cy.get(pageButton).click();
    cy.get(fieldElement).parent().find('.invalid-feedback')
        .should('not.be.hidden')
        .should('have.text', errorMessageUsername);

    cy.get(fieldElement).type(whatToType);
    cy.get(pageButton).click();
    cy.get(fieldElement).parent().find('.invalid-feedback')
        .should('be.hidden');
}

function formatPhoneNumber(value) {
    // if input value is falsy eg if the user deletes the input, then just return
    if (!value) return value;

    // clean the input for any non-digit values.
    const phoneNumber = value.replace(/[^\d]/g, "");

    // phoneNumberLength is used to know when to apply our formatting for the phone number
    const phoneNumberLength = phoneNumber.length;

    // we need to return the value with no formatting if its less then four digits
    // this is to avoid weird behavior that occurs if you  format the area code to early
    if (phoneNumberLength < 4) return phoneNumber;

    // if phoneNumberLength is greater than 4 and less the 7 we start to return
    // the formatted number
    if (phoneNumberLength < 7) {
        return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(3)}`;
    }

    // finally, if the phoneNumberLength is greater then seven, we add the last
    // bit of formatting and return it.
    return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(
        3,
        6
    )}-${phoneNumber.slice(6, 10)}`;
}

describe('As a consumer I want to register self into the Yard Sale application', () => {
    it('When the ROOT page loads, then navigate to the consumer to the Login Register page', () => {
        cy.visit('http://localhost:3000/');
        cy.get('.nav-link').last().should('have.text', 'Login/Register').click();
        cy.url().should('include', '/login');

        cy.get('.loginForm').should('be.visible');

        cy.get('.form-label').first().contains('Username').click();
        cy.get('#username').should('have.focus')
            .should('have.text', '')
            .should('have.attr', 'type', 'text')
            .should('have.attr', 'minLength', '5')
            .should('have.attr', 'maxLength', '5')
            .should('have.attr', 'placeholder', 'Username');

        cy.get('.form-label').last().contains('Password').click();
        cy.get('#password').should('have.focus')
            .should('have.text', '')
            .should('have.attr', 'type', 'password')
            .should('have.attr', 'placeholder', 'Password');

        cy.get('.btn').contains('Login')
            .should('have.text', 'Login');

        cy.get('a.registrationButton').should('have.text', 'Register')
            .should('have.attr', 'href', '/registration');
    })

    it('When Login/Register page loads, then navigate to Register page', () => {
        cy.get('.nav-link').last().should('have.text', 'Login/Register');

        cy.get('a.registrationButton').should('have.text', 'Register')
            .should('have.attr', 'href', '/registration')
            .click();

        cy.url().should('include', '/registration');
    })

    it('When on Register page, check field element Username', () => {
        cy.get('.form-label').first().contains('Username').click();
        cy.get('.form-control').first().should('have.focus');
        cy.get('.form-control').first().should('have.text', '');
        cy.get('.form-control').first().should('have.attr', 'type', 'text');
        cy.get('.form-control').first().should('have.attr', 'placeholder', 'Enter 5 Digit Alias');
    })
    it('When on Register page, check field element Password', () => {
        cy.get('.form-label').eq(1).should('have.text', 'Password').click();
        cy.get('.form-control').eq(1).should('have.focus');
        cy.get('.form-control').eq(1).should('have.text', '');
        cy.get('.form-control').eq(1).should('have.attr', 'type', 'password');
        cy.get('.form-control').eq(1).should('have.attr', 'placeholder', 'Enter A Password');
    })
    it('When on Register page, check field element First Name', () => {
        cy.get('.form-label').eq(2).should('have.text', 'First Name').click();
        cy.get('.form-control').eq(2).should('have.focus');
        cy.get('.form-control').eq(2).should('have.text', '');
        cy.get('.form-control').eq(2).should('have.attr', 'type', 'text');
        cy.get('.form-control').eq(2).should('have.attr', 'placeholder', 'Enter First Name');
    })
    it('When on Register page, check field element Last Name', () => {
        cy.get('.form-label').eq(3).should('have.text', 'Last Name').click();
        cy.get('.form-control').eq(3).should('have.focus');
        cy.get('.form-control').eq(3).should('have.text', '');
        cy.get('.form-control').eq(3).should('have.attr', 'type', 'text');
        cy.get('.form-control').eq(3).should('have.attr', 'placeholder', 'Enter Last Name');
    })
    it('When on Register page, check field element Email', () => {
        cy.get('.form-label').eq(4).should('have.text', 'Email').click();
        cy.get('.form-control').eq(4).should('have.focus');
        cy.get('.form-control').eq(4).should('have.text', '');
        cy.get('.form-control').eq(4).should('have.attr', 'type', 'email');
        cy.get('.form-control').eq(4).should('have.attr', 'placeholder', 'Enter Email Address');
    })
    it('When on Register page, check field element Phone Number', () => {
        cy.get('.form-label').eq(5).should('have.text', 'Phone Number').click();
        cy.get('.form-control').eq(5).should('have.focus');
        cy.get('.form-control').eq(5).should('have.text', '');
        cy.get('.form-control').eq(5).should('have.attr', 'type', 'text');
        cy.get('.form-control').eq(5).should('have.attr', 'placeholder', 'Enter Phone Number');
    })
    it('When on Register page, check field element City', () => {
        cy.get('.form-label').eq(6).should('have.text', 'City').click();
        cy.get('.form-control').eq(6).should('have.focus');
        cy.get('.form-control').eq(6).should('have.text', '');
        cy.get('.form-control').eq(6).should('have.attr', 'type', 'text');
        cy.get('.form-control').eq(6).should('have.attr', 'placeholder', 'Enter City');
    })
    it('When on Register page, check field element State', () => {
        cy.get('.form-label').eq(7).should('have.text', 'State').click();
        cy.get('.form-control').last().should('have.focus');
        cy.get('.form-control').last().should('have.text', '');
        cy.get('.form-control').last().should('have.attr', 'type', 'text');
        cy.get('.form-control').last().should('have.attr', 'placeholder', 'Enter State');
    })
    it('When on Register page, check field element Hub', () => {
        cy.get('.form-label').last().should('have.text', 'Hub').click();
        cy.get('.form-select').should('have.focus');
        cy.get('.form-select').contains('Choose A Hub');
        cy.get('.form-select').select('Atlanta').should('have.value', 'Atlanta');
        cy.get('.form-select').select('Bloomington').should('have.value', 'Bloomington');
        cy.get('.form-select').select('Dallas').should('have.value', 'Dallas');
        cy.get('.form-select').select('Phoenix').should('have.value', 'Phoenix');
        cy.get('.form-select').select('Remote').should('have.value', 'Remote');
    })

    it('When on Register page, check field element Cancel Button', () => {
        cy.get('.btn.btn-secondary').should('have.text', 'Cancel');
        cy.get('.btn.btn-secondary').should('have.attr', 'type', 'button');
    })
    it('When on Register page, check field element Register User Button', () => {
        cy.get('.btn.btn-primary').should('have.text', 'Register User');
        cy.get('.btn.btn-primary').should('have.attr', 'type', 'submit');
    })

    it('When consumer action uses Cancel Button, navigate to Login screen', () => {
        cy.get('.btn.btn-secondary').should('have.text', 'Cancel').click();
        cy.url().should('not.contain', '/registration');
        cy.get('#yardSaleLogo').should('be.visible');
        cy.get('.nav-link').last().should('have.text', 'Login/Register');
    })

    it('A consumer from ROOT page chooses to Register, complete it, and navigate to profile', () => {
        cy.visit('http://localhost:3000/');
        cy.get('.nav-link').last().should('have.text', 'Login/Register').click();
        cy.url().should('include', '/login');

        cy.get('a.registrationButton').should('have.text', 'Register')
            .should('have.attr', 'href', '/registration')
            .click();

        cy.url().should('include', '/registration');

        cy.get('.form-control').first().type(generateRandomAlphaNumericUserID());
        cy.get('.form-control').eq(1).type(gUserID);
        cy.get('.form-control').eq(2).type(fName01);
        cy.get('.form-control').eq(3).type(lName01);
        email01 = gUserID + emaiEndl01;
        cy.get('.form-control').eq(4).type(email01);
        cy.get('.form-control').eq(5).type(pNumber01);
        cy.get('.form-control').eq(6).type(city01);
        cy.get('.form-control').eq(7).type(state01);
        cy.get('.form-select').select('Remote').should('have.value', hub01);

        cy.get('.btn.btn-primary').should('have.text', 'Register User').click();
        cy.url().should('include', '/profile');
    })

    it('When desired, consumer can logout', () => {
        cy.url().should('include', '/profile');
        //        cy.get('.dropdown-toggle nav-link').click();
        cy.get('#collapsible-nav-dropdown').click();
        cy.get('.dropdown-menu > [href="/"]').click();
        cy.url().should('not.include', '/profile');
    })
})

describe('When a consumer tries to navigate to the profile page without login, the app will redirect them to the register/login page', () => {
    it('Will redirect the consumer to the login if not logged in', () => {
        cy.visit('http://localhost:3000/profile');
        cy.wait(1000);
        cy.get('.nav-link').last().should('have.text', 'Login/Register');
        cy.url().should('include', '/login');
    })
})

describe('As a consumer I want to login, check the edit page out, and then logout of the Yard Sale application', () => {
    it('When the ROOT page loads, consumer will Login', () => {
        cy.visit('http://localhost:3000/');
        cy.get('.nav-link').last().should('have.text', 'Login/Register').click();
        cy.url().should('include', '/login');

        cy.get('.loginForm').find('[type="text"]').type(gUserID);
        cy.get('.loginForm').find('[type="password"]').type(gUserID);

        cy.get('#loginSubmit').should('have.text', 'Login').click();

        cy.url().should('include', '/profile');
    })

    it('When logged in, consumer click on edit profile, and verify existing values', () => {
        cy.get('#editContactDetailsButton').should('have.text', 'Edit Account Details').click();

        cy.get('.form-label').first().should('have.text', 'First Name').click();
        cy.get('#createFirstName').should('have.focus')
            .should('have.value', fName01)
            .first().should('have.attr', 'type', 'text')
            .should('have.attr', 'placeholder', 'Enter First Name');

        cy.get('.form-label').eq(1).should('have.text', 'Last Name').click();
        cy.get('#createLastName').should('have.focus')
            .should('have.value', lName01)
            .should('have.attr', 'type', 'text')
            .should('have.attr', 'placeholder', 'Enter Last Name');

        cy.get('.form-label').eq(2).should('have.text', 'Email').click();
        cy.get('#createEmail').should('have.focus')
            .should('have.value', email01)
            .should('have.attr', 'type', 'email')
            .should('have.attr', 'placeholder', 'Enter Email Address');

        cy.get('.form-label').eq(3).should('have.text', 'Phone Number').click();
        cy.get('#createPhone').should('have.focus')
            .should('have.value', formatPhoneNumber(pNumber01))
            .should('have.attr', 'type', 'text')
            .should('have.attr', 'placeholder', 'Enter Phone Number');

        cy.get('.form-label').eq(4).should('have.text', 'City').click();
        cy.get('#createCity').should('have.focus')
            .should('have.value', city01)
            .should('have.attr', 'type', 'text')
            .should('have.attr', 'placeholder', 'Enter City');

        cy.get('.form-label').eq(5).should('have.text', 'State').click();
        cy.get('#createState').should('have.focus')
            .should('have.value', state01)
            .should('have.attr', 'type', 'text')
            .should('have.attr', 'placeholder', 'Enter State');

        cy.get('.form-label').last().should('have.text', 'Hub').click();
        cy.get('#createHub').should('have.focus')
            .should('have.value', hub01);

        cy.get('#editUserInfoSubmit').should('have.text', 'Save Changes')
            .should('have.attr', 'type', 'submit');
        cy.get('.btn.btn-secondary').should('have.text', 'Close')
            .should('have.attr', 'type', 'button');
    })

    it('When On Edit User Details, consumer changes existing values, and saves', () => {
        cy.get('.form-label').first().should('have.text', 'First Name').click();
        cy.get('.form-control').first().should('have.focus');
        cy.get('.form-control').first().should('have.value', fName01);
        cy.get('.form-control').first().type('{selectall}{backspace}').type(efName01);

        cy.get('.form-label').eq(1).should('have.text', 'Last Name').click();
        cy.get('.form-control').eq(1).should('have.focus');
        cy.get('.form-control').eq(1).should('have.value', lName01);
        cy.get('.form-control').eq(1).type('{selectall}').type(elName01);

        cy.get('.form-label').eq(2).should('have.text', 'Email').click();
        cy.get('.form-control').eq(2).should('have.focus');
        cy.get('.form-control').eq(2).should('have.value', email01);
        cy.get('.form-control').eq(2).type('{selectall}').type(eemail01);

        cy.get('.form-label').eq(3).should('have.text', 'Phone Number').click();
        cy.get('.form-control').eq(3).should('have.focus');
        cy.get('.form-control').eq(3).should('have.value', formatPhoneNumber(pNumber01));
        cy.get('.form-control').eq(3).type('{selectall}').type(epNumber01);

        cy.get('.form-label').eq(4).should('have.text', 'City').click();
        cy.get('.form-control').eq(4).should('have.focus');
        cy.get('.form-control').eq(4).should('have.value', city01);
        cy.get('.form-control').eq(4).type('{selectall}').type(ecity01);

        cy.get('.form-label').eq(5).should('have.text', 'State').click();
        cy.get('.form-control').last().should('have.focus');
        cy.get('.form-control').last().should('have.value', state01);
        cy.get('.form-control').last().type('{selectall}').type(estate01);

        cy.get('.form-label').last().should('have.text', 'Hub').click();
        cy.get('.form-select').should('have.focus');
        cy.get('.form-select').should('have.value', hub01);
        cy.get('.form-select').select(ehub01);

        cy.get('#editUserInfoSubmit').should('have.text', 'Save Changes').click();
        cy.url().should('include', '/profile');
    })

    it('When On Edit User Details, consumer changes modified values back to existing, and saves', () => {
        cy.get('#editContactDetailsButton').should('have.text', 'Edit Account Details').click();

        cy.get('.form-label').first().should('have.text', 'First Name').click();
        cy.get('.form-control').first().should('have.focus');
        cy.get('.form-control').first().should('have.value', efName01);
        cy.get('.form-control').first().type('{selectall}{backspace}').type(fName01);

        cy.get('.form-label').eq(1).should('have.text', 'Last Name').click();
        cy.get('.form-control').eq(1).should('have.focus');
        cy.get('.form-control').eq(1).should('have.value', elName01);
        cy.get('.form-control').eq(1).type('{selectall}').type(lName01);

        cy.get('.form-label').eq(2).should('have.text', 'Email').click();
        cy.get('.form-control').eq(2).should('have.focus');
        cy.get('.form-control').eq(2).should('have.value', eemail01);
        cy.get('.form-control').eq(2).type('{selectall}').type(email01);

        cy.get('.form-label').eq(3).should('have.text', 'Phone Number').click();
        cy.get('.form-control').eq(3).should('have.focus');
        cy.get('.form-control').eq(3).should('have.value', formatPhoneNumber(epNumber01));
        cy.get('.form-control').eq(3).type('{selectall}').type(pNumber01);

        cy.get('.form-label').eq(4).should('have.text', 'City').click();
        cy.get('.form-control').eq(4).should('have.focus');
        cy.get('.form-control').eq(4).should('have.value', ecity01);
        cy.get('.form-control').eq(4).type('{selectall}').type(city01);

        cy.get('.form-label').eq(5).should('have.text', 'State').click();
        cy.get('.form-control').last().should('have.focus');
        cy.get('.form-control').last().should('have.value', estate01);
        cy.get('.form-control').last().type('{selectall}').type(state01);

        cy.get('.form-label').last().should('have.text', 'Hub').click();
        cy.get('.form-select').should('have.focus');
        cy.get('.form-select').should('have.value', ehub01);
        cy.get('.form-select').select(hub01);

        cy.get('#editUserInfoSubmit').should('have.text', 'Save Changes').click();
    })

    it('When desired, consumer will logout', () => {
        cy.get('#initialsHeader')
            .should('have.text', efName01.charAt(0) + elName01.charAt(0)).click();
        cy.get('.dropdown-menu > [href="/"]').click();
        cy.url().should('not.include', '/profile');
    })
})

// Learned that browers control validation and type checking does not work
// https://github.com/cypress-io/cypress/issues/6678
describe('Validation checks - login page', () => {
    let lURL = "http://localhost:3000/login";
    let errorMessageUsername = "Did you enter your username correctly?";
    let errorMessagePassword = "Please enter your password";

    it('default login fields - initial page state', () => {
        cy.visit(lURL);

        cy.get('#username').parent().find('.invalid-feedback').should('be.hidden');
        cy.get('#password').parent().find('.invalid-feedback').should('be.hidden');
    })

    it('Will trigger validation errors when submitting empty login fields', () => {
        cy.visit(lURL);

        cy.get('#loginSubmit').click();
        cy.get('#username').parent().find('.invalid-feedback')
            .should('not.be.hidden')
            .should('have.text', errorMessageUsername);
        cy.get('#password').parent().find('.invalid-feedback')
            .should('not.be.hidden')
            .should('have.text', errorMessagePassword);

        cy.url().should('include', '/login')
            .should('not.include', '/profile');
    })

    it('Will trigger validation errors on double click', () => {
        cy.visit(lURL);

        cy.get('#loginSubmit').dblclick();
        cy.get('#username').parent().find('.invalid-feedback')
            .should('not.be.hidden')
            .should('have.text', errorMessageUsername);
        cy.get('#password').parent().find('.invalid-feedback')
            .should('not.be.hidden')
            .should('have.text', errorMessagePassword);

        cy.url().should('include', '/login')
            .should('not.include', '/profile');
    })

    it('Will trigger validation errors when submitting login fields - username focus', () => {
        fieldValidationCheck(lURL, '#username', '#loginSubmit', gUserID, errorMessageUsername);
    })

    it('Will trigger validation errors when submitting login fields - password focus', () => {
        fieldValidationCheck(lURL, '#password', '#loginSubmit', gUserID, errorMessagePassword);
    })
})

// Learned that browers control validation and type checking does not work
// https://github.com/cypress-io/cypress/issues/6678
describe('Validation checks - register/login page', () => {
    let rURL = "http://localhost:3000/registration";
    let errorMessageUsername = "Please provide a valid 5 character username."
    let errorMessagePassword = "Please provide a valid 5+ character password."
    let errorMessageFirstName = "Please enter a First Name."
    let errorMessageLastName = "Please enter a Last Name."
    let errorMessageEmail = "Please provide a valid email address."
    let errorMessagePhone = "Please provide a valid 10 digit phone number."
    let errorMessageCity = "Please enter a City."
    let errorMessageState = "Please enter a State (abbrev.)."
    let errorMessageHub = "Please select a Hub location."

    it('default registration fields - initial page state', () => {
        cy.visit(rURL);

        cy.get('#createUsername').parent().find('.invalid-feedback').should('be.hidden');
        cy.get('#createPassword').parent().find('.invalid-feedback').should('be.hidden');
        cy.get('#createFirstName').parent().find('.invalid-feedback').should('be.hidden');
        cy.get('#createLastName').parent().find('.invalid-feedback').should('be.hidden');
        cy.get('#createEmail').parent().find('.invalid-feedback').should('be.hidden');
        cy.get('#createPhone').parent().find('.invalid-feedback').should('be.hidden');
        cy.get('#createCity').parent().find('.invalid-feedback').should('be.hidden');
        cy.get('#createState').parent().find('.invalid-feedback').should('be.hidden');
        cy.get('#createHub').parent().find('.invalid-feedback').should('be.hidden');
    })

    it('Will trigger validation errors when submitting empty registation fields', () => {
        cy.visit(rURL);

        cy.get('#registerSubmit').click();
        cy.get('#createUsername').parent().find('.invalid-feedback').should('not.be.hidden').should('have.text', errorMessageUsername);
        cy.get('#createPassword').parent().find('.invalid-feedback').should('not.be.hidden').should('have.text', errorMessagePassword);
        cy.get('#createFirstName').parent().find('.invalid-feedback').should('not.be.hidden').should('have.text', errorMessageFirstName);
        cy.get('#createLastName').parent().find('.invalid-feedback').should('not.be.hidden').should('have.text', errorMessageLastName);
        cy.get('#createEmail').parent().find('.invalid-feedback').should('not.be.hidden').should('have.text', errorMessageEmail);
        cy.get('#createPhone').parent().find('.invalid-feedback').should('not.be.hidden').should('have.text', errorMessagePhone);
        cy.get('#createCity').parent().find('.invalid-feedback').should('not.be.hidden').should('have.text', errorMessageCity);
        cy.get('#createState').parent().find('.invalid-feedback').should('not.be.hidden').should('have.text', errorMessageState);
        cy.get('#createHub').parent().find('.invalid-feedback').should('not.be.hidden').should('have.text', errorMessageHub);

        cy.url().should('include', '/registration')
            .should('not.include', '/login')
            .should('not.include', '/profile');
    })

    it('Will trigger validation error when submitting registration fields - username focus', () => {
        fieldValidationCheck(rURL, '#createUsername', '#registerSubmit', gUserID, errorMessageUsername);
    })

    it('Will trigger validation error when submitting registration fields - password focus', () => {
        fieldValidationCheck(rURL, '#createPassword', '#registerSubmit', gUserID, errorMessagePassword);
    })

    it('Will trigger validation error when submitting registration fields - first name focus', () => {
        fieldValidationCheck(rURL, '#createFirstName', '#registerSubmit', fName01, errorMessageFirstName);
    })

    it('Will trigger validation error when submitting registration fields - last name focus', () => {
        fieldValidationCheck(rURL, '#createLastName', '#registerSubmit', lName01, errorMessageLastName);
    })

    it('Will trigger validation error when submitting registration fields - email focus', () => {
        fieldValidationCheck(rURL, '#createEmail', '#registerSubmit', email01, errorMessageEmail);
    })

    it('Will trigger validation error when submitting registration fields - phone focus', () => {
        fieldValidationCheck(rURL, '#createPhone', '#registerSubmit', pNumber01, errorMessagePhone);
    })

    it('Will trigger validation error when submitting registration fields - city focus', () => {
        fieldValidationCheck(rURL, '#createCity', '#registerSubmit', city01, errorMessageCity);
    })

    it('Will trigger validation error when submitting registration fields - state focus', () => {
        fieldValidationCheck(rURL, '#createState', '#registerSubmit', state01, errorMessageState);
    })

    it('Will trigger validation error when submitting registration fields - hub focus', () => {
        cy.visit(rURL);

        cy.get('#registerSubmit').click();
        cy.get('#createHub').parent().find('.invalid-feedback')
            .should('not.be.hidden')
            .should('have.text', errorMessageHub);

        cy.get('#createHub').select(hub01);
        cy.get('#registerSubmit').click();
        cy.get('#createHub').parent().find('.invalid-feedback')
            .should('be.hidden');
    })
})

describe('Validation checks - edit account page', () => {
    let lURL = "http://localhost:3000/login";
    let errorMessageFirstName = "Please enter a First Name."
    let errorMessageLastName = "Please enter a Last Name."
    let errorMessageEmail = "Please provide a valid email address."
    let errorMessagePhone = "Please provide a valid 10 digit phone number."
    let errorMessageCity = "Please enter a City."
    let errorMessageState = "Please enter a State (abbrev.)."
    let errorMessageHub = "Please select a Hub location."

    it('When login page loads, consumer will Login and navigate to edit page', () => {
        cy.visit(lURL);

        cy.get('.loginForm').find('[type="text"]').type(gUserID);
        cy.get('.loginForm').find('[type="password"]').type(gUserID);

        cy.get('#loginSubmit').should('have.text', 'Login').click();
        cy.get('#editContactDetailsButton').should('have.text', 'Edit Account Details').click();
    })

    it('default edit User fields - initial page state', () => {
        cy.get('#createFirstName').parent().find('.invalid-feedback').should('be.hidden');
        cy.get('#createLastName').parent().find('.invalid-feedback').should('be.hidden');
        cy.get('#createEmail').parent().find('.invalid-feedback').should('be.hidden');
        cy.get('#createPhone').parent().find('.invalid-feedback').should('be.hidden');
        cy.get('#createCity').parent().find('.invalid-feedback').should('be.hidden');
        cy.get('#createState').parent().find('.invalid-feedback').should('be.hidden');
        cy.get('#createHub').parent().find('.invalid-feedback').should('be.hidden');
    })

    it('Will trigger validation errors when submitting empty registation fields', () => {
        cy.get('#createFirstName').type('{selectall}{del}');
        cy.get('#createLastName').type('{selectall}{del}');
        cy.get('#createEmail').type('{selectall}{del}');
        cy.get('#createPhone').type('{selectall}{del}');
        cy.get('#createCity').type('{selectall}{del}');
        cy.get('#createState').type('{selectall}{del}');

        cy.get('#editUserInfoSubmit').click();

        cy.get('#createFirstName').parent().find('.invalid-feedback').should('not.be.hidden').should('have.text', errorMessageFirstName);
        cy.get('#createLastName').parent().find('.invalid-feedback').should('not.be.hidden').should('have.text', errorMessageLastName);
        cy.get('#createEmail').parent().find('.invalid-feedback').should('not.be.hidden').should('have.text', errorMessageEmail);
        cy.get('#createPhone').parent().find('.invalid-feedback').should('not.be.hidden').should('have.text', errorMessagePhone);
        cy.get('#createCity').parent().find('.invalid-feedback').should('not.be.hidden').should('have.text', errorMessageCity);
        cy.get('#createState').parent().find('.invalid-feedback').should('not.be.hidden').should('have.text', errorMessageState);
    })

    it('Will trigger validation error when submitting registration fields - first name focus', () => {
        fieldValidationCheck('', '#createFirstName', '#editUserInfoSubmit', fName01, errorMessageFirstName);
    })

    it('Will trigger validation error when submitting registration fields - last name focus', () => {
        fieldValidationCheck('', '#createLastName', '#editUserInfoSubmit', lName01, errorMessageLastName);
    })

    it('Will trigger validation error when submitting registration fields - email focus', () => {
        fieldValidationCheck('', '#createEmail', '#editUserInfoSubmit', email01, errorMessageEmail);
    })

    it('Will trigger validation error when submitting registration fields - phone focus', () => {
        fieldValidationCheck('', '#createPhone', '#editUserInfoSubmit', pNumber01, errorMessagePhone);
    })

    it('Will trigger validation error when submitting registration fields - city focus', () => {
        fieldValidationCheck('', '#createCity', '#editUserInfoSubmit', city01, errorMessageCity);
    })

    it('Will trigger validation error when submitting registration fields - state focus', () => {
        fieldValidationCheck('', '#createState', '#editUserInfoSubmit', state01, errorMessageState);
    })
})

describe('Finish Testing', () => {
    it('Will logut and end testing', () => {
        cy.visit('http://localhost:3000/');
    })
})