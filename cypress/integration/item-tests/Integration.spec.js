/// <reference types="cypress" />

import { hasSelectionSupport } from "@testing-library/user-event/dist/utils";

describe('testing the single item page functionality', () => {

    const title = 'ypressTest Rumbler'
    it('create a new item', () => {
        cy.visit('http://localhost:3000')
        cy.get(':nth-child(2) > .nav-link').click()
        cy.get('#username').type('45678')
        cy.get('#password').type('password')
        cy.get('#loginSubmit').click()
        cy.get('#profileWelcomeLine').should('contain', 'Welcome Captain America')
        cy.get('[href="/items/createitem"]').click()
        cy.get('.createItemTitle').type(title)
        cy.get('.createItemDescription').type('keeps your drinks cold!')
        cy.get('#createItemCategory').select('Tools')
        cy.get('.createItemPrice').clear().type('100')
        cy.get('#createItemContactMethod').select('Email')
        cy.get('#createItemWillingToShip').click()
        cy.get('#createItemPaymentMethod').type('Cash')
        cy.get('#createItemCondition').type('Good')
        cy.get('.choosefile').click()
        cy.get('#createItemSubmitNewItem').click()
        cy.get('#itemPrice').should('contain', '$100')
        
    })

    it('edit newly created item', () => {
        cy.get('#itemPageEditButton').click()
        cy.get('#editItemPrice').clear().type('10')
        cy.get('#editItemSaveButton').click()
        cy.get('#itemPrice').should('contain', '$100')
    })

    it('delete the item', () => {
        cy.get('#itemPageDeleteButton').click()
        cy.get('#profileWelcomeLine').should('contain', 'Welcome Captain America')
    })
})