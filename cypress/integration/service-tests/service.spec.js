/// <reference types="cypress" />

describe('HomePage selections', () => {

    it('show menu options', () => {
        cy.visit('http://localhost:3000/services')
        cy.get('#locSelect').should('have.text', 'Select a Location: ')
        cy.get('#locDropdown').select('Bloomington')
        cy.get('#catSelect').should('have.text', 'Select a Category:')
    })

    it('shows post for a selected category', () => {
        cy.get('.selectedCat').click()
        cy.get('.serviceList').should('have.length', 1)
    })

    it('checks if sorting buttons are clickable', () => {
        cy.get('.AZButton').should('not.be.disabled')
        cy.get('.ZAButton').should('not.be.disabled')
        cy.get('.UndoButton').should('not.be.disabled')
    })

    it('checks if required fields are not empty', () => {
        cy.get('.titleDescription').should('not.be.empty')
        cy.get('.categoryDescription').should('not.be.empty')
        cy.get('.serviceDescription').should('not.be.empty')
        cy.get('.locationDetail').should('not.be.empty')
    })

    it('checks if a post show all other fields', () => {
        cy.get('.serviceRangeDetail').should('contain', 'Service Range:')
        cy.get('.priceDetail').should('contain', 'Price:')
        cy.get('.availabilityDetail').should('contain', 'Availability:')
        cy.get('.yrsOfExpDetail').should('contain', 'Years of Experience:')
        cy.get('.pmtMethodDetaill').should('contain', 'Payment Methods Accepted:')
        cy.get('.preferredContactMethodDetail').should('contain', 'Preferred Contact Methods:')
        cy.get('.emailDetail').should('contain', 'Email Address:')
        cy.get('.phoneDetail').should('contain', 'Phone Number:')
    })
})

describe('Homepage - test the "create new post" button', () => {

    it('checks if it routes to a different page', () => {
        cy.visit('http://localhost:3000/login')
        cy.get('#username').type('jasbi')
        cy.get('#password').type('jasbilang')
        cy.get('button').contains('Login').click()
        cy.url().should('eq', 'http://localhost:3000/profile')
        cy.get('a').contains('Services').click()
        cy.get('.App-link-button').contains('Create new post').click()
        cy.url().should('eq', 'http://localhost:3000/services/AddNewPost')
    })
})

describe('Homepage - test if keyword search works', () => {

    it('checks if it returns the search', () => {
        cy.visit('http://localhost:3000/services')
        cy.get('#keywordClick').contains('Keyword Search').click()
        cy.get('.searchbox').type('piano')
        cy.get('.titleDescription').contains('Piano')
    })
})

describe('test functionality of the AddNewPost page', () => {

    it('creates a new post', () => {
        cy.visit('http://localhost:3000/login')
        cy.get('#username').type('jasbi')
        cy.get('#password').type('jasbilang')
        cy.get('button').contains('Login').click()
        cy.url().should('eq', 'http://localhost:3000/profile')
        cy.get('a').contains('Services').click()
        cy.get('.App-link-button').contains('Create new post').click()
        cy.get('#titleDesc').type('Fun Cypress')
        cy.get('#descDetail').type('creating new post')
        cy.get('#catOption').select('Cleaning')
        cy.get('#locOption').select('Bloomington')
        cy.get('#price').type('10')
        cy.get('#unit').select('Weekly')      
        cy.get('#availability').type('M, W, F')
        cy.get('#yrsOfExp').type('5')
        cy.get('#serviceRange').type('2')
        cy.get('#pmtMethod').select('PayPal')
        cy.get('#contactMethod').select('Text Message')
        // cy.get('#email').type('test@service.com')
        // cy.get('#phone').type('123-456-7899')
        cy.get('#publish').click({force: true})
    })
})

describe('test edit functionality', () => {

    it('edit a post', () => {
        cy.visit('http://localhost:3000/login')
        cy.get('#username').type('jasbi')
        cy.get('#password').type('jasbilang')
        cy.get('button').contains('Login').click()
        cy.url().should('eq', 'http://localhost:3000/profile')
        cy.get('#tabListingId-tab-Service').contains('Service').click()
        cy.get('button').contains('Active Listings').click({force: true})
        cy.get('#editProfileButton').click({force: true})
        cy.get('#titleDesc').type(' Testing')
        cy.get('#publish').click()
    })
})

describe('test delete functionality', () => {

    it('deletes a post', () => {
        cy.visit('http://localhost:3000/login')
        cy.get('#username').type('jasbi')
        cy.get('#password').type('jasbilang')
        cy.get('button').contains('Login').click()
        cy.url().should('eq', 'http://localhost:3000/profile')
        cy.get('a').contains('Services').click()
        cy.get('#keywordClick').contains('Keyword Search').click()
        cy.get('.searchbox').type('cypress').wait(5000)
        cy.get('#mainPageEditButton').click()
        cy.get('#deleteButton').contains('Delete').click()
        cy.get('#confirmDelete').click()
        cy.get('.searchbox').type('cypress')
        cy.get('.card').should('not.exist');
    })
})


