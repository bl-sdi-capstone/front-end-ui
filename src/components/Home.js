import React, { useState } from 'react';
import '../styles/Home.css';
import { useNavigate } from 'react-router-dom';
import { Form, Button, Image } from 'react-bootstrap';

function Home() {
    const { search } = window.location;
    const query = new URLSearchParams(search).get('s');
    const [searchQuery, setSearchQuery] = useState(query || '');
    const [listingSelect, setListingSelect] = useState('Items')
    const navigate = useNavigate();

    const onInputChange = (e) => {
        setListingSelect(e.target.value)
    }

    function onSubmit(e) {
        e.preventDefault();
        if (listingSelect === 'Items') {
            navigate(`/items/?s=${searchQuery}`);
        } else {
            navigate(`/services/?s=${searchQuery}`);
        }
    }

    return (
        <>
            <div className="homeContent">
                <Image fluid
                    id="yardSaleLogo"
                    src={require("../images/YardSaleLogoType_Final.png")}
                    alt="Yard Sale"
                />
                <Form action="/" method="get" onSubmit={onSubmit} className="homeSearchForm">
                    <Form.Group controlId="formGridSelect">
                        <Form.Select name="listingSelect" size='lg' value={listingSelect} onChange={onInputChange}>
                            <option key="Items" value="Items">Items</option>
                            <option key="Services" value="Services">Services</option>
                        </Form.Select>
                    </Form.Group>
                    <Form.Group controlId="formGridSearch">
                        <Form.Control
                            value={searchQuery}
                            onInput={e => setSearchQuery(e.target.value)}
                            type="text"
                            placeholder="Search listings"
                            name="s"
                            size='lg'
                        />
                    </Form.Group>
                    <Form.Group controlId="formGridSubmit">
                        <Button variant="item_primary" id="homeSearchButton" type="submit" size='lg'>Search</Button>
                    </Form.Group>
                </Form>
            </div>
        </>
    )
}

export default Home