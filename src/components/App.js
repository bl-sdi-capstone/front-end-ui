import { useContext, useReducer } from "react";
import { Outlet } from "react-router-dom";
import AppContext from "../contexts/AppContext";
import AuthContext from "../contexts/AuthContext";
import "../styles/App.css";


const initialState = {
  url: "http://aae7c78e5e3954b3e8042f4d9023d58f-346942550.us-west-2.elb.amazonaws.com/api/users",
  registerURL: "http://auth.galvanizelaboratory.com/api/account/register",
  userInfo: {},
  token: "",
  username: "",
};

function reducer(state, action) {
  switch (action.type) {
    case "initialLoad":
    case "editPatch":
    case "registration":
    case "savePicture":
      return { ...state, userInfo: action.payload };
    case "token":
      return { ...state, token: action.payload };
    case "username":
      return { ...state, username: action.payload };
    default:
      return { ...state };
  }
}

export default function App() {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [authState, authDispatch] = useContext(AuthContext);

  return (
    <AuthContext.Provider value={[authState, authDispatch]}>
      <AppContext.Provider value={[state, dispatch]}>
        <div className="App">
          <Outlet />
        </div>
      </AppContext.Provider>
    </AuthContext.Provider>
  );
}