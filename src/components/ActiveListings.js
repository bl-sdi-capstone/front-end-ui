import React from "react";
import "../styles/App.css";
import { Accordion } from "react-bootstrap";
import UsersActiveServices from "../Services/Components/UsersActiveServices";
import ServiceContext from "../Services/Contexts/ServiceContext";

export default function ActiveListings({ userDetails }) {
  return (
    <>
      <Accordion className="ActiveListings">
        <Accordion.Item eventKey="0">
          <Accordion.Header>Active Listings</Accordion.Header>
          <Accordion.Body>
            <div className="panel-body-service">
              {/* <div className="t1-service">
                <div className="t2-service"> */}
                  <UsersActiveServices />
                {/* </div>
              </div> */}
            </div>
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
    </>
  );
}
