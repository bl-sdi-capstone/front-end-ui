import React, { useEffect, useContext } from "react";
import AppContext from "../contexts/AppContext.js";
import AuthContext from "../contexts/AuthContext.js";
import "../styles/App.css";
import UserInfo from "./UserInfo";
import CreateListing from "./CreateListing";
import ActiveListings from "./ActiveListings";
import PastListings from "./PastListings";
import { Tabs } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import ActiveUserItems from "./ActiveUserItems.js";
import PastUserItems from "./PastUserItems.js";

export default function Profile() {
  const [state, dispatch] = useContext(AppContext);
  const [authState, authDispatch] = useContext(AuthContext);

  let navigate = useNavigate();

  useEffect(() => {

    if (authState.initialized === true) {

      if (authState.token === undefined) {
        navigate("/login")
      } else {
        fetch(state.url + "/" + authState.username, {
          headers: {
            "Content-Type": "application/json",
            Authorization: authState.token,
          },
        })
          .then((response) => {
            if (response.status === 404) {

              alert(
                "Failed to pull your credentials due to missing registration information. Click Ok to fill in missing information."
              );

              return navigate("/registration");
            } else {
              return response.json();
            }
          })
          .then((content) => {
            dispatch({ type: "initialLoad", payload: content });
          })
      };
    }
  }, [authState.username]);

  return (

    <div className="profileBackround">
      <div id="Profile">
        <UserInfo userDetails={state.userInfo ? state.userInfo : {}} />
        <CreateListing userDetails={state.userInfo ? state.userInfo : {}} />
        <Tabs
          defaultActiveKey="Item"
          id="tabListingId"
          className="tabListingClassName"
        >
          <Tabs id="try" eventKey="Item" title="Item">
            <ActiveUserItems />
            <PastUserItems />
          </Tabs>
          <Tabs eventKey="Service" title="Service">
            <ActiveListings userDetails={state.userInfo ? state.userInfo : {}} />
            <PastListings userDetails={state.userInfo ? state.userInfo : {}} />
          </Tabs>
        </Tabs>
      </div>
    </div>

  );
}