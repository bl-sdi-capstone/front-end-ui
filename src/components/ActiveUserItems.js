import React from "react";
import "../styles/App.css";
import { Accordion } from "react-bootstrap";
import ItemsActive from "../Items/ItemComponents/ItemListByUser/ItemsActive";

export default function ActiveUserItems() {
  return (
    <>
      <Accordion className="ActiveListings">
        <Accordion.Item eventKey="0">
          <Accordion.Header>Active Listings</Accordion.Header>
          <Accordion.Body>
            <div className="panel-body">
              <div className="t1">
                <div className="t2">
                  <ItemsActive />
                </div>
              </div>
            </div>
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
    </>
  );
}
