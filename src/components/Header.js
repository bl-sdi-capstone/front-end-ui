import React, { useContext, useEffect, useState, useRef } from "react";
import { Navbar, Container, Nav, NavDropdown, Image } from "react-bootstrap";
import AuthContext from "../contexts/AuthContext";
import { Link, useNavigate } from "react-router-dom";
import '../styles/Header.css';
import { useCookies } from 'react-cookie';
import UserContext from "../contexts/UserContext";

const userInitials = {
  firstName: "",
  lastName: "",
  profilePicture: "",
}

export default function Header() {

  const [authState, authDispatch] = useContext(AuthContext);
  const [userState, userDispatch] = useContext(UserContext);

  const [name, setName] = useState(userInitials);

  const [initialized, setInitialized] = useState(false);
  
  const [cookies, setCookie, removeCookie] = useCookies(["Name", "Token"]);

  const imageURL = "http://aae7c78e5e3954b3e8042f4d9023d58f-346942550.us-west-2.elb.amazonaws.com/api"

  const [profilePic, setProfilePic] = useState(imageURL + "/images/" + userState.userInfo.profilePicture)
  
  const fName = (userState.userInfo.firstName !== undefined ? userState.userInfo.firstName : "")
  const lName = (userState.userInfo.lastName !== undefined ? userState.userInfo.lastName : "")
  const proPic = (userState.userInfo.profilePicture !== undefined ? userState.userInfo.profilePicture : "")

  const initials = fName.charAt(0) + lName.charAt(0);
  
  useEffect(() => {
    if (userState.userInfo.firstName !== undefined || userState.userInfo.firstName !== "") {
      if (fName !== "") {
        setName(fName, lName, proPic)
        setProfilePic(imageURL + "/images/" + userState.userInfo.profilePicture)
        console.log("starting to initialize")
        setInitialized(true);
      }
    }
  }, [userState.userInfo])
  
  let navigate = useNavigate();
  
  const logout = () => {
    
    authDispatch({ type: 'logout' });
    userDispatch({ type: 'logout' });
    removeCookie("Name", { path: '/' })
    removeCookie("Token", { path: '/' })
    alert("You logged out!")
    navigate("/")
    
  };
  
  const bgColor = useRef(null);
    
    const usernameToColor = () => {
        let name = authState.username;
        let hash = 0;
        for (let i = 0; i < name.length; i++) {
            hash = name.charCodeAt(i) + ((hash << 5) - hash);
        }
  
        let h = hash % 360;
        let s = 30;
        let l = 80;
        return 'hsl('+h+','+s+'%, '+l+'%)';
    }
  
    const updateColor = () => {
        let textColor = '#555';
  
        let hexColor = usernameToColor();
  
        bgColor.current.style.background = hexColor;
        bgColor.current.style.color = textColor;
    }
  
  
    useEffect(() => {
      if(bgColor.current !== null) {
        if(authState.username !== undefined || authState.username !== "") {
          updateColor();
        }
      }
    }, [authState.username]);
    
  const profileImage = (
    (userState.userInfo.profilePicture === "" || userState.userInfo.profilePicture === undefined) ?
    <div ref={bgColor} id="initialsHeader">{initials}</div>
    :
      <Image
        src={(profilePic !== undefined ? imageURL + "/images/" + userState.userInfo.profilePicture : profilePic)}
        alt="UserName profile image"
        roundedCircle
        style={{ width: '40px', height: '40px' }}
      />
  )


  return (
    <>
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Container>
          <Navbar.Brand >
            <Nav.Link as={Link} to="/">
              <img
                id="yardSaleHeaderLogo"
                src={require("../images/YardSaleLogoType_Final.png")}
                alt="Yard Sale"
              />
            </Nav.Link>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link id='itemsNav' as={Link} to="/items">Items</Nav.Link>
              <Nav.Link as={Link} to="/services">Services</Nav.Link>
            </Nav>
            <Nav>{authState.token ? (
              <NavDropdown title={profileImage} id="collapsible-nav-dropdown" className="removeCaret">
                <NavDropdown.Item as={Link} to="/profile" >Profile</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item as={Link} to="/items/createitem">
                  Create Item Listing
                </NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/services/AddNewPost">
                  Create Service Listing
                </NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item as={Link} to="/" onClick={logout}>
                  Logout
                </NavDropdown.Item>
              </NavDropdown>
            ) : (
              <Nav.Link as={Link} to="/login">Login/Register</Nav.Link>
            )}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}
