import React, { useState } from "react";
import "../styles/App.css";
import { Modal, Button } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";

export default function CreateListing() {
  return (
    <>
      <Button
        id="createAListingButton"
        className="CreateListing"
        as={Link}
        to="/items/createitem"
      >
        Create an Item Listing
      </Button>
      <Button
        id="createAListingButton"
        className="CreateListing"
        as={Link}
        to="/services/AddNewPost"
      >
        Create a Service Listing
      </Button>
    </>
  );
}
