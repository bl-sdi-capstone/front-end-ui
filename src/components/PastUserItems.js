import React from "react";
import "../styles/App.css";
import { Accordion } from "react-bootstrap";
import ItemsInactive from "../Items/ItemComponents/ItemListByUser/ItemsInactive";

export default function PastUserItems() {
  return (
    <Accordion className="PastListings">
      <Accordion.Item eventKey="0">
        <Accordion.Header>Inactive Listings</Accordion.Header>
        <Accordion.Body>
          <div className="panel-body">
            <div className="t1">
              <div className="t2">
                <ItemsInactive />
              </div>
            </div>
          </div>
        </Accordion.Body>
      </Accordion.Item>
    </Accordion>
  );
}
