import React from "react";
import "../styles/App.css";
import { Accordion } from "react-bootstrap";
import UsersInactiveServices from "../Services/Components/UsersInactiveServices";

export default function PastListings({ userDetails }) {
  return (
    <Accordion className="PastListings">
      <Accordion.Item eventKey="0">
        <Accordion.Header>Inactive Listings</Accordion.Header>
        <Accordion.Body>
          <div className="panel-body-service">
            {/* <div className="t1-service">
              <div className="t2-service"> */}
                <UsersInactiveServices />
              {/* </div>
            </div> */}
          </div>
        </Accordion.Body>
      </Accordion.Item>
    </Accordion>
  );
}
