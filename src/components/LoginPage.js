import React, { useState, useContext } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import AuthContext from "../contexts/AuthContext";
import { useNavigate, Link } from "react-router-dom";
import { useCookies } from "react-cookie";
import { FaEye, FaEyeSlash } from "react-icons/fa";

export default function LoginPage() {
  const initialStateLogin = {
    username: "",
    password: "",
  };

  const [cookies, setCookie] = useCookies([]);
  const [authState, authDispatch] = useContext(AuthContext);
  const [currentLogin, setCurrentLogin] = useState(initialStateLogin);
  const [userCred, setUserCred] = useState("");
  const [passwordShown, setPasswordShown] = useState(false);
  const [validated, setValidated] = useState(false);

  const url = "http://auth.galvanizelaboratory.com/api/auth";
  
  let navigate = useNavigate();

  const onInputChangeLogin = (e) => {
    setCurrentLogin({ ...currentLogin, [e.target.name]: e.target.value });
  };

  const toInputLowercase = (e) => {
    let value = e.target.value;
    value = value.toLowerCase();
    setCurrentLogin({ ...currentLogin, [e.target.name]: value });
  };

  const togglePassword = () => {
    setPasswordShown(!passwordShown);
  };

  // Login
  const sendLoginRequest = (e) => {
    const form = e.currentTarget;
    setValidated(true);
    e.preventDefault();
    if (!form.checkValidity()) {
      e.stopPropagation();
    } else {
      fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(currentLogin),
      })
        .then((response) => {
          if (!response.ok) {
            alert("Failed to login. Please check your credentials.");
          } else {
            return response.headers;
          }
        })
        .then((data) => {
          if (data) {
            setUserCred(data.get("Authorization"));
            setCookie("Token", data.get("Authorization"), { path: "/" });
            authDispatch({ type: "token", payload: data.get("Authorization") });

            setCookie("Name", currentLogin.username, { path: "/" });
            authDispatch({ type: "username", payload: currentLogin.username });
          }
        })
        .then(() => {
          navigate("/profile");
        })
        .catch((e) => {
          window.location.reload(false);
        });
    }
  };

  return (
    <>
      <div id="loginFormCenter">
        <Form
          className="loginForm"
          noValidate
          validated={validated}
          onSubmit={sendLoginRequest}
        >
          <Row className="mb-3">
            <Form.Group as={Row} md="4">
              <Form.Label htmlFor="username">
                <strong>Username</strong>
              </Form.Label>
              <Form.Control
                required
                type="text"
                name="username"
                id="username"
                placeholder="Username"
                minLength="5"
                maxLength="5"
                value={currentLogin.username}
                onChange={toInputLowercase}
                autoFocus
              />
              <Form.Control.Feedback type="invalid">
                Did you enter your username correctly?
              </Form.Control.Feedback>
            </Form.Group>
          </Row>
          <Row className="mb-4">
            <Form.Group as={Row} md="3">
              <Form.Label htmlFor="password">
                <strong>Password</strong>
              </Form.Label>
              <Form.Floating>
                <i className="showPasswordLogin" onClick={togglePassword}>
                  {passwordShown ? <FaEye /> : <FaEyeSlash />}
                </i>
              </Form.Floating>
              <Form.Control
                required
                type={passwordShown ? "text" : "password"}
                name="password"
                id="password"
                placeholder="Password"
                value={currentLogin.password}
                onChange={onInputChangeLogin}
              />
              <Form.Control.Feedback type="invalid">
                Please enter your password
              </Form.Control.Feedback>
            </Form.Group>
          </Row>
          <Row>
            <Col>
              <Button size="md" id="loginSubmit" type="submit">
                Login
              </Button>
            </Col>
            <Col>
              <Link className="registrationButton" to="/registration">
                Register
              </Link>
            </Col>
          </Row>
        </Form>
      </div>
    </>
  );
}
