import React, { useContext, useEffect, useRef, useState } from 'react';
import '../styles/App.css';
import { Button, Image, Modal, Form } from "react-bootstrap";
import AuthContext from '../contexts/AuthContext';
import AppContext from '../contexts/AppContext';
import { MdAddAPhoto } from 'react-icons/md';
import UserContext from '../contexts/UserContext';

const initialImageState = {
    firstName: "",
    lastName: "",
    profilePicture: "",
}


export default function ProfilePicture({ firstName, lastName, profilePicture }) {

    
    const [authState, authDispatch] = useContext(AuthContext);
    
    const [state, dispatch] = useContext(AppContext);

    const [userState, userDispatch] = useContext(UserContext);

    const [currImage, setCurrImage] = useState(initialImageState);

    const [validImage, setValidImage] = useState(false);

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const imageURL = "http://aae7c78e5e3954b3e8042f4d9023d58f-346942550.us-west-2.elb.amazonaws.com/api"

    useEffect(() => {
        setCurrImage(firstName, lastName, profilePicture);
    }, [firstName, lastName, profilePicture]);

    const initials = firstName.charAt(0) + lastName.charAt(0);

    const chooseImage = (e) => {
        let sizeValidation = e.target.files[0]
        if (sizeValidation.size > 1e6) {
            alert("Your image is greater than 1 MB, please select another.")
        } else {
            setValidImage(true);
            setCurrImage(e.target.files[0]);
        }
    }

    const saveImage = (e) => {
        e.preventDefault();
        if (validImage === true) {
            const formData = new FormData();
            formData.append('multipartImage', currImage);
            fetch(imageURL + "/users/" + authState.username + "/image", {
                method: "POST",
                headers: {
                    'Authorization': authState.token
                },
                body: formData,
            })
                .then(response => {
                    if (!response.ok) {
                        alert("Something went wrong!  Please try again.")
                        throw "Something went wrong!"
                    } else {
                        return response.json();
                    }
                }).then(content => {
                    dispatch({ type: "savePicture", payload: content });
                    userDispatch({ type: "savePicture", payload: content});
                    handleClose();
                }).catch((e) => {
                    handleClose();
                })
        } else {
            alert("Your image is greater than 1 MB, please select another.")
        }
    }
    
    const bgColor = useRef(null);
    
    const usernameToColor = () => {
        let name = authState.username;
        let hash = 0;
        for (let i = 0; i < name.length; i++) {
            hash = name.charCodeAt(i) + ((hash << 5) - hash);
        }

        let h = hash % 360;
        let s = 30;
        let l = 80;
        return 'hsl('+h+','+s+'%, '+l+'%)';
    }

    const updateColor = () => {
        let textColor = '#555';

        let hexColor = usernameToColor();

        bgColor.current.style.background = hexColor;
        bgColor.current.style.color = textColor;
    }


    useEffect(() => {
        updateColor();
    }, [authState.username]);

    return (
        <>
            <div>
                {profilePicture === "" ?
                    <div ref={bgColor} id="profileImage" >{initials}</div> :
                    <Image id="profileImage" src={imageURL + "/images/" + profilePicture} />}

            </div>

            {/* <Button className="uploadImageButton" onClick={() => handleShow()}> */}
            <MdAddAPhoto as="Button" className="uploadImage" onClick={() => handleShow()} />
            {/* </Button> */}

            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard="false"
                id="editImageModal"
                dialogClassName="modal-w"
            >
                <Modal.Header closeButton>
                    <Modal.Title>Upload a new Image</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={saveImage}>
                        <Form.Group controlId="formFile" className="mb-3">
                            <Form.Label>Default file input example</Form.Label>
                            <Form.Control type="file" onChange={chooseImage} />
                            <Button className="modalImageBtn" variant="secondary" onClick={() => handleClose()}>Close</Button>
                            <Button className="modalImageBtn" id="editUserInfoSubmit" type="submit" variant="primary">Save Changes</Button>
                        </Form.Group>
                    </Form>
                </Modal.Body>
            </Modal>
        </>
    );
}

// {ProfilePicture} Profile Picture URL (Defaults to Outlook but allows for user upload)