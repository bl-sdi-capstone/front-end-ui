import React, { useState, useContext, useEffect, useRef } from "react";
import { Form, Button, Col, Row } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import AppContext from "../contexts/AppContext";
import AuthContext from "../contexts/AuthContext";
import { FaEye, FaEyeSlash } from "react-icons/fa";

const initialStateNewUser = {
  username: "",
  password: "",
  firstName: "",
  lastName: "",
  email: "",
  phone: "",
  hub: "",
  city: "",
  state: "",
};

export default function Registration() {
  const [state, dispatch] = useContext(AppContext);
  const [authState, authDispatch] = useContext(AuthContext);

  const [userCred, setUserCred] = useState("");
  const [currentUser, setCurrentUser] = useState(initialStateNewUser);
  const [phoneInputValue, setPhoneInputValue] = useState("");
  const [passwordShown, setPasswordShown] = useState(false);
  const [validated, setValidated] = useState(false);

  const url = "http://auth.galvanizelaboratory.com/api/auth";

  const form = useRef(null);

  let navigate = useNavigate();

  const togglePassword = () => {
    setPasswordShown(!passwordShown);
  };

  useEffect(() => {
    if (authState.username) {
      setCurrentUser({ ...currentUser, username: authState.username });
    }
  }, [authState.username]);


  const onInputChange = (e) => {
    setCurrentUser({ ...currentUser, [e.target.name]: e.target.value });

    if (form.current.checkValidity() === true) {
      setValidated(true);
    } else {
      setValidated(false);
    }
  };

  const toInputLowercase = (e) => {
    let value = e.target.value
    value = value.replace(/[^a-zA-Z0-9]/ig, '')
    value = value.toLowerCase();
    setCurrentUser({ ...currentUser, [e.target.name]: value });
    if (form.current.checkValidity() === true) {
      setValidated(true);
    } else {
      setValidated(false);
    }
  };

  const onInputChangeAlpha = (e) => {
    let value = e.target.value
    value = value.replace(/[^A-Za-z\.\-\'\s]/ig, '');
    setCurrentUser({ ...currentUser, [e.target.name]: value });
    if (form.current.checkValidity() === true) {
      setValidated(true);
    } else {
      setValidated(false);
    }
  };

  const toInputUppercase = (e) => {
    let value = e.target.value
    value = value.replace(/[^A-Za-z]/ig, '')
    value = value.toUpperCase();
    setCurrentUser({ ...currentUser, [e.target.name]: value });
    if (form.current.checkValidity() === true) {
      setValidated(true);
    } else {
      setValidated(false);
    }
  };

  const handlePhoneNumber = (e) => {
    // this is where we'll call the phoneNumberFormatter function
    const formattedPhoneNumber = formatPhoneNumber(e.target.value);
    // we'll set the input value using our setPhoneInputValue
    setPhoneInputValue(formattedPhoneNumber);

    // Update currentUser
    const phoneNumber = formattedPhoneNumber.replace(/[^\d]/g, "");
    setCurrentUser({ ...currentUser, [e.target.name]: phoneNumber });
  };

  function formatPhoneNumber(value) {
    // if input value is falsy eg if the user deletes the input, then just return
    if (!value) return value;

    // clean the input for any non-digit values.
    const phoneNumber = value.replace(/[^\d]/g, "");

    // phoneNumberLength is used to know when to apply our formatting for the phone number
    const phoneNumberLength = phoneNumber.length;

    // we need to return the value with no formatting if its less then four digits
    // this is to avoid weird behavior that occurs if you  format the area code to early
    if (phoneNumberLength < 4) return phoneNumber;

    // if phoneNumberLength is greater than 4 and less the 7 we start to return
    // the formatted number
    if (phoneNumberLength < 7) {
      return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(3)}`;
    }

    // finally, if the phoneNumberLength is greater then seven, we add the last
    // bit of formatting and return it.
    return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(
      3,
      6
    )}-${phoneNumber.slice(6, 10)}`;
  }

  const checkTokenPath = (e) => {
    const form = e.currentTarget;
    setValidated(true);
    e.preventDefault();
    if (!form.checkValidity()) {
      e.stopPropagation();
    } else {
      checkUserInfo();
    }
  };

  const checkUserInfo = () => {
    if (validated === true) {
      if (authState.token) {
        postNewUser(authState.token);
      } else {
        registerTask();
      }
    }
  };

  const registerTask = (e) => {
    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(currentUser),
    };
    fetch(state.registerURL, options)
      .then((response) => {
        if (!response.ok) {
          alert("Something went wrong.  Please try again.")
          throw "Something went wrong with Registration"
        } else {
          return currentUser;
        }
      })
      .then((data) => {
        dispatch({ type: "registration", payload: data });
        getToken();
      }).catch((e) => {
        console.log(e);
        navigate("/")
      });
  };

  const getToken = () => {
    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: currentUser.username,
        password: currentUser.password,
      }),
    };
    fetch(url, options)
      .then((response) => {
        return response.headers;
      })
      .then((data) => {
        setUserCred(data.get("Authorization"));
        postNewUser(data.get("Authorization"));
      });
  };

  function postNewUser(authToken) {
    fetch(state.url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: authToken,
      },
      body: JSON.stringify(currentUser),
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
      })
      .then((data) => {
        dispatch({ type: "registration", payload: data });
        authDispatch({ type: "token", payload: authToken });
        authDispatch({ type: "username", payload: currentUser.username })
        navigate("/profile");
      });
  }

  return (
    <>
      <div>
        <Form id="registrationForm" onSubmit={checkTokenPath} noValidate validated={validated} ref={form} className="mx-auto">
          {authState.token ? (
            ``
          ) : (
            <>
              <Row className="mb-3">
                <Form.Group as={Col} md="4">
                  <Col>
                    <Form.Label htmlFor="createUsername">Username</Form.Label>
                  </Col>
                  <Col xs="auto">
                    <Form.Control
                      required
                      type="text"
                      name="username"
                      id="createUsername"
                      placeholder="Enter 5 Digit Alias"
                      minLength="5"
                      maxLength="5"
                      value={currentUser.username}
                      onChange={toInputLowercase}
                      autoFocus
                    />
                    <Form.Control.Feedback type="invalid">
                      Please provide a valid 5 character username.
                    </Form.Control.Feedback>
                  </Col>
                </Form.Group>
                <Form.Group as={Col} md="4">
                  <Col>
                    <Form.Label htmlFor="createPassword">Password<i onClick={togglePassword}>{passwordShown ? <FaEye /> : <FaEyeSlash />}</i>
</Form.Label>
                  </Col>
                  <Col>
                    <Form.Control
                      required
                      type={passwordShown ? "text" : "password"}
                      name="password"
                      id="createPassword"
                      placeholder="Enter A Password"
                      minLength="5"
                      value={currentUser.password}
                      onChange={onInputChange}
                    />
                    <Form.Control.Feedback type="invalid">
                      Please provide a valid 5+ character password.
                    </Form.Control.Feedback>
                  </Col>
                </Form.Group>
              </Row>
            </>
          )}
          <Row className="mb-3">
            <Form.Group as={Col} md="4" >
              <Col>
                <Form.Label htmlFor="createFirstName">First Name</Form.Label>
              </Col>
              <Col>
                <Form.Control
                  required
                  type="text"
                  name="firstName"
                  id="createFirstName"
                  placeholder="Enter First Name"
                  value={currentUser.firstName}
                  onChange={onInputChangeAlpha}
                />
                <Form.Control.Feedback type="invalid">
                  Please enter a First Name.
                </Form.Control.Feedback>
              </Col>
            </Form.Group>
            <Form.Group as={Col} md="4" >
              <Col>
                <Form.Label htmlFor="createLastName">Last Name</Form.Label>
              </Col>
              <Col>
                <Form.Control
                  required
                  type="text"
                  name="lastName"
                  id="createLastName"
                  placeholder="Enter Last Name"
                  value={currentUser.lastName}
                  onChange={onInputChangeAlpha}
                />
                <Form.Control.Feedback type="invalid">
                  Please enter a Last Name.
                </Form.Control.Feedback>
              </Col>
            </Form.Group>
          </Row>
          <Row className="mb-3">
            <Form.Group as={Col} md="5" >
              <Col>
                <Form.Label htmlFor="createEmail">Email</Form.Label>
              </Col>
              <Col>
                <Form.Control
                  required
                  type="email"
                  name="email"
                  id="createEmail"
                  placeholder="Enter Email Address"
                  value={currentUser.email}
                  onChange={onInputChange}
                />
                <Form.Control.Feedback type="invalid">
                  Please provide a valid email address.
                </Form.Control.Feedback>
              </Col>
            </Form.Group>
            <Form.Group as={Col} md="3" >
              <Col>
                <Form.Label htmlFor="createPhone">Phone Number</Form.Label>
              </Col>
              <Col>
                <Form.Control
                  required
                  type="text"
                  name="phone"
                  id="createPhone"
                  placeholder="Enter Phone Number"
                  value={phoneInputValue}
                  onChange={(e) => handlePhoneNumber(e)}
                />
                <Form.Control.Feedback type="invalid">
                  Please provide a valid 10 digit phone number.
                </Form.Control.Feedback>
              </Col>
            </Form.Group>
          </Row>
          <Row className="mb-3">
            <Form.Group as={Col} md="3" >
              <Col>
                <Form.Label htmlFor="createCity">City</Form.Label>
              </Col>
              <Col>
                <Form.Control
                  required
                  type="text"
                  name="city"
                  id="createCity"
                  placeholder="Enter City"
                  value={currentUser.city}
                  onChange={onInputChangeAlpha}
                />
                <Form.Control.Feedback type="invalid">
                  Please enter a City.
                </Form.Control.Feedback>
              </Col>
            </Form.Group>
            <Form.Group as={Col} md="2" >
              <Col>
                <Form.Label htmlFor="createState">State</Form.Label>
              </Col>
              <Col>
                <Form.Control
                  required
                  type="text"
                  name="state"
                  id="createState"
                  placeholder="Enter State"
                  minLength="2"
                  maxLength="2"
                  value={currentUser.state}
                  onChange={toInputUppercase}
                />
                <Form.Control.Feedback type="invalid">
                  Please enter a State (abbrev.).
                </Form.Control.Feedback>
              </Col>
            </Form.Group>
            <Form.Group as={Col} md="3" >
              <Col>
                <Form.Label htmlFor="createHub">Hub</Form.Label>
              </Col>
              <Col>
                <Form.Select
                  required
                  name="hub"
                  id="createHub"
                  value={currentUser.hub}
                  onChange={onInputChange}
                >
                  <option hidden value="">Choose A Hub</option>
                  <option value="Atlanta">Atlanta</option>
                  <option value="Bloomington">Bloomington</option>
                  <option value="Dallas">Dallas</option>
                  <option value="Phoenix">Phoenix</option>
                  <option value="Remote">Remote</option>
                </Form.Select>
                <Form.Control.Feedback type="invalid">
                  Please select a Hub location.
                </Form.Control.Feedback>
              </Col>
            </Form.Group>
          </Row>
          <div id="registerCancel">
          <Button
            id="registerSubmit"
            variant="primary"
            type="submit"
            className="regButtons"
          >
            Register User
          </Button>
          <Button  variant="secondary" onClick={() => navigate("/")} className="regButtons">
            Cancel
          </Button>
          </div>
        </Form>
      </div>
    </>
  );
}
