import React, { useContext, useState, useEffect, useRef } from "react";
import AppContext from "../contexts/AppContext";
import ProfilePicture from "./ProfilePicture.js";
import "../styles/App.css";
import Container from "react-bootstrap/Container";
import { Modal, Form, Button, Row, Col } from "react-bootstrap";
import UserContext from "../contexts/UserContext";

const initialStateEditUser = {
  firstName: "",
  lastName: "",
  email: "",
  phone: "",
  hub: "",
  city: "",
  state: "",
};

export default function UserInfo({ userDetails }) {
  const [state, dispatch] = useContext(AppContext);
  const [userState, userDispatch] = useContext(UserContext);
  const [currentUser, setCurrentUser] = useState(initialStateEditUser);
  const [phoneInputValue, setPhoneInputValue] = useState("");
  const [validated, setValidated] = useState(false);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const form = useRef(null);

  useEffect(() => {
    setCurrentUser(userDetails);
    let fpn = formatPhoneNumber(userDetails.phone);
    setPhoneInputValue(fpn);
  }, [userDetails]);

  const onInputChange = (e) => {
    setCurrentUser({ ...currentUser, [e.target.name]: e.target.value });
    if (form.current.checkValidity() === true) {
      setValidated(true);
    } else {
      setValidated(false);
    }
  };

  const onInputChangeAlpha = (e) => {
    let value = e.target.value;
    value = value.replace(/[^A-Za-z\.\-\'\s]/gi, "");
    setCurrentUser({ ...currentUser, [e.target.name]: value });
    if (form.current.checkValidity() === true) {
      setValidated(true);
    } else {
      setValidated(false);
    }
  };

  const saveUpdate = (e) => {
    const form = e.currentTarget;
    setValidated(true);
    e.preventDefault();
    if (!form.checkValidity()) {
      e.stopPropagation();
    } else {
      patchTask();
    }
  };

  function patchTask() {
    fetch(state.url + "/" + userDetails.username, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(currentUser),
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
      })
      .then((data) => {
        dispatch({ type: "editPatch", payload: data });
        userDispatch({ type: "editPatch", payload: data });
      })
      .then(setShow(false));
  }

  const toInputUppercase = (e) => {
    let value = e.target.value;
    value = value.replace(/[^A-Za-z]/gi, "");
    value = value.toUpperCase();
    setCurrentUser({ ...currentUser, [e.target.name]: value });
    if (form.current.checkValidity() === true) {
      setValidated(true);
    } else {
      setValidated(false);
    }
  };

  const handlePhoneNumber = (e) => {
    // this is where we'll call the phoneNumberFormatter function
    const formattedPhoneNumber = formatPhoneNumber(e.target.value);
    // we'll set the input value using our setPhoneInputValue
    setPhoneInputValue(formattedPhoneNumber);

    // Update currentUser
    const phoneNumber = formattedPhoneNumber.replace(/[^\d]/g, "");
    setCurrentUser({ ...currentUser, [e.target.name]: phoneNumber });
  };

  function formatPhoneNumber(value) {
    // if input value is falsy eg if the user deletes the input, then just return
    if (!value) return value;

    // clean the input for any non-digit values.
    const phoneNumber = value.replace(/[^\d]/g, "");

    // phoneNumberLength is used to know when to apply our formatting for the phone number
    const phoneNumberLength = phoneNumber.length;

    // we need to return the value with no formatting if its less then four digits
    // this is to avoid weird behavior that occurs if you  format the area code to early
    if (phoneNumberLength < 4) return phoneNumber;

    // if phoneNumberLength is greater than 4 and less the 7 we start to return
    // the formatted number
    if (phoneNumberLength < 7) {
      return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(3)}`;
    }

    // finally, if the phoneNumberLength is greater then seven, we add the last
    // bit of formatting and return it.
    return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(
      3,
      6
    )}-${phoneNumber.slice(6, 10)}`;
  }

  return (
    <>
      <div id="profileDiv">
        <ProfilePicture
          className="ProfilePicture"
          firstName={userDetails.firstName ? userDetails.firstName : ""}
          lastName={userDetails.lastName ? userDetails.lastName : ""}
          profilePicture={
            userDetails.profilePicture ? userDetails.profilePicture : ""
          }
        />
      </div>
      <h2 id="profileWelcomeLine">
        Welcome{" "}
        {state.userInfo
          ? userDetails.firstName +
            " " +
            userDetails.lastName +
            " (" +
            userDetails.username +
            ")"
          : "!"}!
      </h2>
      <Button
        id="editContactDetailsButton"
        className="Button"
        onClick={() => handleShow()}
      >
        Edit Account Details
      </Button>

      <Modal
        centered
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard="false"
        id="editUserInfoModal"
        dialogClassName="modal-w"
      >
        <Modal.Header closeButton>
          <Modal.Title>Edit User Info</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form
            noValidate
            validated={validated}
            onSubmit={saveUpdate}
            ref={form}
          >
            <Row className="mb-3">
              <Form.Group as={Col} md="6">
                <Col>
                  <Form.Label htmlFor="createFirstName">First Name</Form.Label>
                </Col>
                <Col>
                  <Form.Control
                    required
                    type="text"
                    name="firstName"
                    id="createFirstName"
                    placeholder="Enter First Name"
                    value={currentUser.firstName}
                    onChange={onInputChangeAlpha}
                    autoFocus
                  />
                  <Form.Control.Feedback type="invalid">
                    Please enter a First Name.
                  </Form.Control.Feedback>
                </Col>
              </Form.Group>
              <Form.Group as={Col} md="6">
                <Col>
                  <Form.Label htmlFor="createLastName">Last Name</Form.Label>
                </Col>
                <Col>
                  <Form.Control
                    required
                    type="text"
                    name="lastName"
                    id="createLastName"
                    placeholder="Enter Last Name"
                    value={currentUser.lastName}
                    onChange={onInputChangeAlpha}
                  />
                  <Form.Control.Feedback type="invalid">
                    Please enter a Last Name.
                  </Form.Control.Feedback>
                </Col>
              </Form.Group>
            </Row>
            <Row className="mb-3">
              <Form.Group as={Col} md="8">
                <Col>
                  <Form.Label htmlFor="createEmail">Email</Form.Label>
                </Col>
                <Col>
                  <Form.Control
                    required
                    type="email"
                    name="email"
                    id="createEmail"
                    placeholder="Enter Email Address"
                    value={currentUser.email}
                    onChange={onInputChange}
                  />
                  <Form.Control.Feedback type="invalid">
                    Please provide a valid email address.
                  </Form.Control.Feedback>
                </Col>
              </Form.Group>
              <Form.Group as={Col} md="4">
                <Col>
                  <Form.Label htmlFor="createPhone">Phone Number</Form.Label>
                </Col>
                <Col>
                  <Form.Control
                    required
                    type="text"
                    name="phone"
                    id="createPhone"
                    placeholder="Enter Phone Number"
                    value={phoneInputValue}
                    onChange={(e) => handlePhoneNumber(e)}
                  />
                  <Form.Control.Feedback type="invalid">
                    Please provide a valid 10 digit phone number.
                  </Form.Control.Feedback>
                </Col>
              </Form.Group>
            </Row>
            <Row className="mb-3">
              <Form.Group as={Col} md="5">
                <Col>
                  <Form.Label htmlFor="createCity">City</Form.Label>
                </Col>
                <Col>
                  <Form.Control
                    required
                    type="text"
                    name="city"
                    id="createCity"
                    placeholder="Enter City"
                    value={currentUser.city}
                    onChange={onInputChangeAlpha}
                  />
                  <Form.Control.Feedback type="invalid">
                    Please enter a City.
                  </Form.Control.Feedback>
                </Col>
              </Form.Group>
              <Form.Group as={Col} md="2">
                <Col>
                  <Form.Label htmlFor="createState">State</Form.Label>
                </Col>
                <Col>
                  <Form.Control
                    required
                    type="text"
                    name="state"
                    id="createState"
                    placeholder="Enter State"
                    minLength="2"
                    maxLength="2"
                    value={currentUser.state}
                    onChange={toInputUppercase}
                  />
                  <Form.Control.Feedback type="invalid">
                    Please enter a State (abbrev.).
                  </Form.Control.Feedback>
                </Col>
              </Form.Group>
              <Form.Group as={Col} md="5">
                <Col>
                  <Form.Label htmlFor="createHub">Hub</Form.Label>
                </Col>
                <Col>
                  <Form.Select
                    required
                    name="hub"
                    id="createHub"
                    value={currentUser.hub}
                    onChange={onInputChange}
                  >
                    <option hidden value="">
                      Choose Hub Location
                    </option>
                    <option value="Atlanta">Atlanta</option>
                    <option value="Bloomington">Bloomington</option>
                    <option value="Dallas">Dallas</option>
                    <option value="Phoenix">Phoenix</option>
                    <option value="Remote">Remote</option>
                  </Form.Select>
                  <Form.Control.Feedback type="invalid">
                    Please select a Hub location.
                  </Form.Control.Feedback>
                </Col>
              </Form.Group>
            </Row>
            <Button variant="secondary" onClick={() => handleClose()}>
              Close
            </Button>
            <Button id="editUserInfoSubmit" type="submit" variant="primary">
              Save Changes
            </Button>
          </Form>
        </Modal.Body>
      </Modal>

      <Container fluid="sm">
        <Row xs>
          <Col xs></Col>
          <Col xs>
            <h5 className="profileDetailsAlignmentLeftColumn">
              <strong>--Contact Details--</strong>
            </h5>
            <ul className="profileDetailsAlignmentLeftColumn">
              <strong>Email: </strong>
              {userDetails.email}
            </ul>
            <ul className="profileDetailsAlignmentLeftColumn">
              <strong>Phone: </strong>
              {formatPhoneNumber(userDetails.phone)}
            </ul>
          </Col>
          <Col xs>
            <h5 className="profileDetailsAlignmentRightColumn">
              <strong>--Location--</strong>
            </h5>
            <ul className="profileDetailsAlignmentRightColumn">
              <strong>City: </strong>
              {userDetails.city}
            </ul>
            <ul className="profileDetailsAlignmentRightColumn">
              <strong>State: </strong>
              {userDetails.state}
            </ul>
            <ul className="profileDetailsAlignmentRightColumn">
              <strong>Hub: </strong>
              {userDetails.hub}
            </ul>
          </Col>
          <Col xs></Col>
        </Row>
      </Container>
    </>
  );
}
