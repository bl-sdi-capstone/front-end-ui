import { useReducer, useEffect, useState } from "react";
import { Routes, Route, useNavigate, useLocation } from 'react-router-dom';
import App from './components/App';
import AuthContext from "./contexts/AuthContext";
import LoginPage from './components/LoginPage';
import Profile from './components/Profile';
import ItemList from './Items/ItemComponents/ItemList/ItemList';
import CreateItem from './Items/ItemComponents/CreateItem/CreateItem';
import ItemPage from './Items/ItemComponents/ItemPage/ItemPage';
import ItemApp from './Items/ItemApp';
import ServiceApp from './Services/ServiceApp';
import ServiceHome from './Services/Components/ServiceHome';
import Registration from "./components/Registration.js";
import UserInfo from "./components/UserInfo";
import ServicePostNew from "./Services/Components/ServicePostNew";
import Header from "./components/Header";
import Home from "./components/Home"
import { useCookies } from 'react-cookie';
import EditService from "./Services/Components/EditService";
import UserContext from "./contexts/UserContext";


const authInitialState = {
    token: "",
    username: "",
    initialized: false,
};

const authReducer = (state, action) => {
    /* Login, Logout cases */
    switch (action.type) {
        case "init":
            return {...state, initialized: true};
        case "token":
            return { ...state, token: action.payload };
        case "username":
            return { ...state, username: action.payload };
        case "logout":
            return { ...state, username: "", token: "" };
        default:
            return { ...state };
    }
}

const userInitialState = {
    url: "http://aae7c78e5e3954b3e8042f4d9023d58f-346942550.us-west-2.elb.amazonaws.com/api/users",
    userInfo: {},
}

const userReducer = (state, action) => {
    switch (action.type) {
        case "loadUser":
        case "savePicture":
        case "editPatch":
            return {...state, userInfo: action.payload};
        case "logout":
            return {...state, userInfo: {}};
        default:
            return {...state};    
    }
}


export default function Router(props) {
    const [rootPage, setRootPage] = useState(true);
    const [authState, authDispatch] = useReducer(authReducer, authInitialState);
    const [userState, userDispatch] = useReducer(userReducer, userInitialState);

    const [cookies, setCookie, removeCookie] = useCookies([]);

    const location = useLocation();

    useEffect (() => {

        authDispatch({ type: 'token', payload: cookies.Token });
        authDispatch({ type: 'username', payload: cookies.Name });
        authDispatch({ type: 'init'});

    }, [cookies.Name, cookies.Token])

    useEffect(() => {
        setRootPage(location.pathname === "/" )
    }, [location])

    useEffect(() => {
        if (authState.token === undefined) {
          return;
        } else if (authState.token === "") {
            return;
        } else {
        fetch(userState.url + "/" + authState.username, {
          headers: {
            "Content-Type": "application/json",
            Authorization: authState.token,
          },
        })
          .then((response) => {
            if (response.status === 404) {
    
              alert(
                "Something Bad Happened."
              );
    
              return;
            } else {
              return response.json();
            }
          })
          .then((content) => {
            userDispatch({ type: "loadUser", payload: content });
          })};
      }, [authState.username]);

    return (
        <AuthContext.Provider value={[authState, authDispatch]}>
            <UserContext.Provider value={[userState, userDispatch]}>
            { rootPage ? <div className="bgImage"></div> : '' }
            <Header />
            <Routes>
                <Route path="/" element={<App />}>
                    <Route path="/" element={<Home />}></Route>
                    <Route path="profile" element={<Profile />}></Route>
                    <Route path="registration" element={<Registration />}></Route>
                    <Route path="login" element={<LoginPage />}></Route>
                    <Route path="edit" element={<UserInfo />}></Route>
                </Route>
                <Route path='/items' element={<ItemApp />}>
                    <Route path='' element={<ItemList />}></Route>
                    <Route path='/items/ItemPage/:itemID' element={<ItemPage />}></Route>
                    <Route path='/items/createitem' element={<CreateItem />}></Route>
                </Route>
                <Route path='/services' element={<ServiceApp />}>
                    <Route path='' element={<ServiceHome />}></Route>
                    <Route path='/services/editPage/:serviceID' element={<EditService/>}></Route>
                    <Route path='/services/AddNewPost' element={<ServicePostNew />}></Route>
                </Route>
            </Routes>
            </UserContext.Provider>
        </AuthContext.Provider>
    )
}