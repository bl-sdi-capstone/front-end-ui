import { Outlet } from 'react-router-dom';
import './ItemApp.css';
import { useReducer, useEffect, useContext } from 'react';
import ItemContext from './Contexts/ItemContext';
import AuthContext from '../contexts/AuthContext';

const initialState = {
  shipping: false,
  filter: [],
  itemList: [],
  dropDownData: {
    category: [],
    condition: [],
    contactMethod: [],
    location: [],
    paymentMethod: [],
    status: []
  },
  apiUrl: "aa27a8c98f8194ff0948f5272a9c3855-1365164240.us-west-2.elb.amazonaws.com"
  // apiUrl:"localhost:8080"
};

function reducer(state, action) {
  switch (action.type) {
    case 'shipping':
      return { ...state, shipping: action.payload };
    case 'setItemList':
      return { ...state, itemList: action.payload };
    case 'setDropdownData':
      return { ...state, dropDownData: action.payload };
    case 'addItem':
      return { ...state, itemList: [...state.itemList, action.payload] }
    case 'delete':
      const updatedItemList = state.itemList.filter(item => { return item.id !== action.payload })
      return { ...state, itemList: updatedItemList }
    case 'edit':
      let updatedList = state.itemList.map(item => item.id === action.id ? action.payload : item)
      return { ...state, itemList: updatedList }
    default:
      return state;
  }
}

function ItemApp() {
  const [state, dispatch] = useReducer(reducer, initialState);
  // User Authentication State
  const [authState, authDispatch] = useContext(AuthContext);
  const dropdownUrl = `http://${state.apiUrl}/api/seeder`

  useEffect(() => {
    fetch(dropdownUrl).then((response) => {
      return response.json();
    }).then((data) => {
      dispatch({ type: 'setDropdownData', payload: data })
    })
  }, []);

  return (
    <AuthContext.Provider value={[authState, authDispatch]} >
      <ItemContext.Provider value={[state, dispatch]}>
        <div className="App">
          <Outlet />
        </div>
      </ItemContext.Provider>
    </AuthContext.Provider >
  );
}

export default ItemApp;