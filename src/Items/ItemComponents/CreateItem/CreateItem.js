import React, { useState, useContext, useEffect } from 'react';
import { Form, InputGroup, FormControl, Button } from 'react-bootstrap';
import ItemContext from '../../Contexts/ItemContext';
import { useNavigate } from 'react-router-dom';
import AuthContext from '../../../contexts/AuthContext';
import UserContext from '../../../contexts/UserContext';
import { Container, Row, Col } from 'react-bootstrap';
import './CreateItemStyles.css'

const initialStateNewItem = {
  "title": "",
  "category": "Tools",
  "price": "0",
  "location": "Atlanta",
  "contactMethod": "Email",
  "phoneNumber": "",
  "picture": "1",
  "emailAddress": "",
  "willingToShip": false,
  "paymentMethod": "Cash",
  "condition": "Good",
  "description": "",
  "status": "Active"
}

function CreateItem() {
  //general Item state
  const [state, dispatch] = useContext(ItemContext);
  // User Authentication State
  const [authState,] = useContext(AuthContext);
  // User Info State
  const [userState,] = useContext(UserContext)
  const [currentImage, setCurrentImage] = useState({});
  const [submitForm, setSubmitForm] = useState(false);
  const [currentItem, setCurrentItem] = useState(initialStateNewItem);
  const [validated, setValidated] = useState(false);
  const navigate = useNavigate();
  const dropDown = state.dropDownData
  const userInfo = userState.userInfo
  const [phoneInputValue, setPhoneInputValue] = useState("");
  const [textAreaCount, setTextAreaCount] = useState(0);
  const [titleTextCount, setTitleTextCount] = useState(0);
  const itemsURL = `http://${state.apiUrl}/api/items`
  const dropdownUrl = `http://${state.apiUrl}/api/seeder`

  const onInputChange = (e) => {
    if (e.target.name === "description") {
      setTextAreaCount(e.target.value.length);
    }
    if (e.target.name === "title") {
      setTitleTextCount(e.target.value.length);
    }
    setCurrentItem({ ...currentItem, [e.target.name]: e.target.value });
  }

  const imageHandler = (e) => {
    setCurrentImage(e.target.files[0]);
  }

  const addItem = (e) => {
    e.preventDefault();
    const form = e.currentTarget;

    if (form.checkValidity() === false) {
      e.preventDefault();
      e.stopPropagation();
    }
    setValidated(true);

    if (form.checkValidity()) {
      const addedItem = {...currentItem}
      addedItem.status = e.nativeEvent.submitter.name
      if (addedItem.shipping === "true") {
        addedItem.shipping = true;
      } else {
        addedItem.shipping = false;
      }
      const options = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': authState.token
        },
        body: JSON.stringify(addedItem)
      };
      fetch(itemsURL, options).then((response) => {
        return response.json();
      }).then((data) => {
        const formData = new FormData();
        formData.append('multipartImage', currentImage);
        const imageURL = `http://${state.apiUrl}/api/items/${data.id}/image`
        fetch(imageURL, {
          method: "POST",
          headers: {
            'Authorization': authState.token
          },
          body: formData,
        }).then(response => {
          dispatch({ type: 'addItem', payload: addedItem })
          navigate(`/items/ItemPage/${data.id}`);
        });
      })
    }
  }

  useEffect(() => {
    fetch(dropdownUrl).then((response) => {
      return response.json();
    }).then((data) => {
      dispatch({ type: 'setDropdownData', payload: data })
    })
  }, []);

  useEffect(() => {
    setCurrentItem({ ...currentItem, phoneNumber: userInfo.phone, emailAddress: userInfo.email, location: userInfo.hub })
    setPhoneInputValue(formatPhoneNumber(userInfo.phone))
  }, [userState]);

  const handlePhoneNumber = (e) => {
    // this is where we'll call the phoneNumberFormatter function
    const formattedPhoneNumber = formatPhoneNumber(e.target.value);
    // we'll set the input value using our setPhoneInputValue
    setPhoneInputValue(formattedPhoneNumber);

    // Update currentUser
    const phoneNumber = formattedPhoneNumber.replace(/[^\d]/g, "");
    setCurrentItem({ ...currentItem, [e.target.name]: phoneNumber });
  };

  function formatPhoneNumber(value) {
    // if input value is falsy eg if the user deletes the input, then just return
    if (!value) return value;

    // clean the input for any non-digit values.
    const phoneNumber = value.replace(/[^\d]/g, "");

    // phoneNumberLength is used to know when to apply our formatting for the phone number
    const phoneNumberLength = phoneNumber.length;

    // we need to return the value with no formatting if its less then four digits
    // this is to avoid weird behavior that occurs if you  format the area code to early
    if (phoneNumberLength < 4) return phoneNumber;

    // if phoneNumberLength is greater than 4 and less the 7 we start to return
    // the formatted number
    if (phoneNumberLength < 7) {
      return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(3)}`;
    }

    // finally, if the phoneNumberLength is greater then seven, we add the last
    // bit of formatting and return it.
    return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(3, 6)}-${phoneNumber.slice(6, 10)}`;
  }

  function navigateBack() {
    window.history.back();
  }

  return (
    <>
      <Container className="border border-light" fluid id='container' >
        <Form onSubmit={addItem} noValidate validated={validated} className='scroll' id='row-box'>
          <Form.Text ></Form.Text>
          <h1 id='item-main-header'>Item Information</h1>
          <Form.Group>
            <Row >
              <Col>
                <Form.Label id='item-ind-title'>Title</Form.Label>
                <Form.Control required placeholder="Please enter an item title" type="text" name="title" className='createItemTitle' controlid='createItemTitle' maxLength="24" value={currentItem.title} onChange={onInputChange}></Form.Control>
                <span className={titleTextCount > 19 ? "itemTitleCounterRed" : "itemTitleCounter"}>{titleTextCount}/24</span>
                <Form.Control.Feedback type="invalid" ></Form.Control.Feedback>
              </Col>
              <Col>
                <Form.Label id='item-ind-title'>Price</Form.Label>
                <InputGroup>
                  <InputGroup.Text>$</InputGroup.Text>
                  <FormControl name="price" className='createItemPrice' controlid='createItemPrice' type="number" min="0" value={currentItem.price} onChange={onInputChange} />
                  <Form.Control.Feedback >Your item price is ${currentItem.price}.</Form.Control.Feedback>
                </InputGroup>
              </Col>
            </Row>
          </Form.Group>

          <Form.Group>
            <Row>
              <Col>
                <Form.Label id='item-ind-title'>Category</Form.Label>
                <Form.Select name="category" id='createItemCategory' value={currentItem.category} onChange={onInputChange}>
                  {dropDown.category.length > 0 ? dropDown.category.map(cat => {
                    return (
                      <option key={cat.name} value={cat.name}>{cat.name}</option>
                    )
                  }) : <option>Loading</option>}
                </Form.Select>
                <Form.Control.Feedback >Your item category is {currentItem.category}.</Form.Control.Feedback>
              </Col>

              <Col>
                <Form.Label id='item-ind-title'>Condition</Form.Label>
                <Form.Select name="condition" id='createItemCondition' value={currentItem.condition} onChange={onInputChange}>
                  {dropDown.condition.length > 0 ? dropDown.condition.map(cond => {
                    return (
                      <option key={cond.name} value={cond.name}>{cond.name}</option>
                    )
                  }) : <option>Loading</option>}
                </Form.Select>
                <Form.Control.Feedback >Your item condition is {currentItem.condition}.</Form.Control.Feedback>
              </Col>
            </Row>
          </Form.Group>

          <Form.Group controlid="formFile" className="mb-3" >
            <Row>
              <Form.Label id='item-ind-title'>Upload Picture</Form.Label>
              <Form.Control className='choosefile' controlid='choosefile' type="file" name="picture" onChange={imageHandler} />
              <Form.Control.Feedback >If no file is chosen, a default image will appear.</Form.Control.Feedback>
            </Row>
            <Row>
              <Form.Label id='item-ind-title'>Description</Form.Label>
              <Form.Control required className='createItemDescription' name="description" controlid='createItemDescription' value={currentItem.description} onChange={onInputChange} as="textarea" maxLength="255" placeholder="Please enter a description"></Form.Control>
              <span className={textAreaCount > 225 ? "itemDescriptionCounterRed" : "itemDescriptionCounter"}>{textAreaCount}/255</span>
              <Form.Control.Feedback type="invalid" ></Form.Control.Feedback>
            </Row>
          </Form.Group>

          <Form.Group id='exchanges'>
            <Form.Label id='item-ind-title-exchange'>Select an Exchange Option</Form.Label>
            <Form.Check inline type="radio" value={true} name="willingToShip" id='createItemWillingToShip' label="Willing to ship" onClick={onInputChange}></Form.Check>
            <Form.Check inline type="radio" value={false} name="willingToShip" id='createItemNotWillingToShip' label="Pickup Only" onClick={onInputChange}></Form.Check>
          </Form.Group>

          <h1 id='item-main-header'>Seller Information</h1>
          <Form.Group>
            <Row>
              <Col>
                <Form.Label id='item-ind-title'>Preferred Payment</Form.Label>
                <Form.Select required name="paymentMethod" id='createItemPaymentMethod' value={currentItem.paymentMethod} onChange={onInputChange}>
                  {dropDown.paymentMethod.length > 0 ? dropDown.paymentMethod.map(pay => {
                    return (
                      <option key={pay.name} value={pay.name}>{pay.name}</option>
                    )
                  }) : <option>Loading</option>}
                </Form.Select>
                <Form.Control.Feedback >Preferred payment method is {currentItem.paymentMethod}.</Form.Control.Feedback>
              </Col>
              <Col>
                <Form.Label id='item-ind-title'>Preferred Contact</Form.Label>
                <Form.Select required name="contactMethod" id='createItemContactMethod' value={currentItem.contactMethod} onChange={onInputChange}>
                  {dropDown.contactMethod.length > 0 ? dropDown.contactMethod.map(con => {
                    return (
                      <option key={con.name} value={con.name}>{con.name}</option>
                    )
                  }) : <option>Loading</option>}
                </Form.Select>
                <Form.Control.Feedback >Preferred contact method is {currentItem.contactMethod}.</Form.Control.Feedback>
              </Col>
            </Row>
          </Form.Group>

          <Form.Group>
            <Row>
              <Col>
                <Form.Label id='item-ind-title'>Phone</Form.Label>
                <Form.Control required pattern="\([0-9]{3}\) [0-9]{3}-[0-9]{4}"
                  type="tel" placeholder="xxx-xxx-xxxx" name="phoneNumber" className='createItemPhoneNumber' controlid='createItemPhoneNumber' value={phoneInputValue} onChange={(e) => handlePhoneNumber(e)}></Form.Control>
                <Form.Control.Feedback type="invalid" >Please enter phone with dashes</Form.Control.Feedback>
              </Col>
              <Col>
                <Form.Label id='item-ind-title'>Email</Form.Label>
                <Form.Control required placeholder="user@email.com" type="email" name="emailAddress" className='createItemEmailAddress' controlid='createItemEmailAddress' value={currentItem.emailAddress} onChange={onInputChange}></Form.Control>
                <Form.Control.Feedback type="invalid" >Please enter a valid email address</Form.Control.Feedback>
              </Col>
            </Row>
          </Form.Group>

          <Button variant="secondary" id='cancelNewItem' onClick={navigateBack}>Cancel</Button>
          <Button type='submit' name='Inactive' id='createItemDraft'>Save as Draft</Button>
          <Button type='submit' name='Active' variant="item_primary" id='createItemSubmitNewItem'>Publish</Button>
        </Form>
      </Container>
    </>
  )
}

export default CreateItem;