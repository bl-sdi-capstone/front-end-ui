import React, { useEffect, useContext } from 'react'
import { Card, Container, Button } from 'react-bootstrap'
import { useState } from 'react'
import { Link } from 'react-router-dom'
import AuthContext from '../../../contexts/AuthContext';
import './ItemsByUserStyles.css'

export default function ItemsInactive() {
    const [authState,] = useContext(AuthContext);
    const [inactiveList, setInactiveList] = useState([]);
    const inactiveItemsURL = `http://aa27a8c98f8194ff0948f5272a9c3855-1365164240.us-west-2.elb.amazonaws.com/api/items/inactive/${authState.username}`;
    // const inactiveItemsURL = `http://localhost:8080/api/items/inactive/${authState.username}`;

    function loadPage() {
        fetch(inactiveItemsURL).then((response) => {
            if (response.ok && response.status !== 204) {
                return response.json();
            }
        }).then((data) => {
            setInactiveList([data]);
        })
    }

    useEffect(() => {
        if (!authState.token == "") {
            loadPage();
        }
    }, [authState.token]);

    const shipItem = (item) => {
        if (item.willingToShip === true) {
            return "Shipping Available";
        } else {
            return "Pick-up Only";
        }
    }

    return (
        <>
            <Container id='item-list-page-main-column' fluid >
                {inactiveList.length > 0 && inactiveList[0] !== undefined ? inactiveList[0].itemList.map(item => {
                    return (
                        <Card id='item-list-page-card' key={item.id} >
                            <Card.Img id='item-list-page-card-image' variant="top" src={`http://aa27a8c98f8194ff0948f5272a9c3855-1365164240.us-west-2.elb.amazonaws.com/api/items/${item.id}/image`} />
                            {/* <Card.Img id='item-list-page-card-image' variant="top" src={`http://localhost:8080/api/items/${item.id}/image`} /> */}
                            <Card.Body>
                                <Card.Title>{item.title}</Card.Title>
                                <Card.Text>{item.price === 0 ? "Free" : `$${item.price}`} </Card.Text>
                                <Card.Text> {item.location} {shipItem(item)} </Card.Text>
                                <Link to={`/items/ItemPage/${item.id}`}>
                                    <Button variant="item_primary" type="button">View this Item</Button>
                                </Link>
                            </Card.Body>
                        </Card>
                    )
                }) : "No items found"}
            </Container>
        </>
    )
}