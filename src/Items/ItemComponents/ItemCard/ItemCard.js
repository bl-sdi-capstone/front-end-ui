import React, { useContext } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import ItemContext from '../../Contexts/ItemContext';
import './ItemCard.css'

const ItemCard = ({ item }) => {
    const [state, dispatch] = useContext(ItemContext);

    const shipItem = (item) => {
        if (item.willingToShip === true) {
            return "Shipping Available";
        } else {
            return "Pick-up Only";
        }
    }

    return (
        <>
            <Card id='item-list-page-card' key={item.id} >
                {item.picture === undefined ? "Picture Loading" : <Card.Img id='item-list-page-card-image' variant="top" src={`http://${state.apiUrl}/api/images/${item.picture}`} />}
                <Card.Body>
                    <Card.Title>{item.title}</Card.Title>
                    <Card.Text>{item.price === 0 ? "Free" : `$${item.price}`} </Card.Text>
                    <Card.Text> {item.location} {shipItem(item)} </Card.Text>
                    <Link to={`/items/ItemPage/${item.id}`}>
                        <Button variant="item_primary" type="button">View this Item</Button>
                    </Link>
                </Card.Body>
            </Card>
        </>
    );
}

export default ItemCard;