import React from 'react';
import { Card, Container } from 'react-bootstrap';
import './CondorCard.css'

const CondorCard = () => {
    let teamsInfo = [
        {
            id: 1,
            name: "Brandon",
            location: "Phoenix",
            photo: 206
        },
        {
            id: 2,
            name: "Brendan",
            location: "Bloomington",
            photo: 221
        },
        {
            id: 3,
            name: "Dannon",
            location: "Dallas",
            photo: 228
        },
        {
            id: 4,
            name: "Jessi",
            location: "Bloomington",
            photo: 230
        },
    ]

    return (
        <>
            <h2 id='devCardModalTitle'>The East Bay Andean Condors</h2>
            <Container id="dev-card-container">
                {teamsInfo.map(dev => {
                    return (
                        <Card id='dev-card' key={dev.id + dev.name} >
                            <Card.Img id='dev-image' variant="top" src={`http://aa27a8c98f8194ff0948f5272a9c3855-1365164240.us-west-2.elb.amazonaws.com/api/images/${dev.photo}`} />
                            <Card.Body>
                                <Card.Title>{dev.name}</Card.Title>
                                <Card.Text> {dev.location} </Card.Text>
                            </Card.Body>
                        </Card>
                    )
                })}
            </Container>
        </>
    );
}

export default CondorCard;
