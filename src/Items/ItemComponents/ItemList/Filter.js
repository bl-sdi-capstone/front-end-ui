import React, { useContext, useEffect } from 'react'
import { Form, Button, InputGroup, Navbar, FormControl } from 'react-bootstrap'
import ItemContext from '../../Contexts/ItemContext'
import './ItemListStyles.css'

function Filter({ setFilterObject, filterObject, setFilterList, filterList }) {
    const [state, dispatch] = useContext(ItemContext);
    const dropDown = state.dropDownData;
    const dropdownUrl = `http://${state.apiUrl}/api/seeder`

    useEffect(() => {
        fetch(dropdownUrl).then((response) => {
            return response.json();
        }).then((data) => {
            dispatch({ type: 'setDropdownData', payload: data })
        })
    }, []);


    function shippingFilter() {
        let shipping = !filterObject.shipping
        setFilterObject({ ...filterObject, shipping: shipping })
        let copiedFilterList = [...filterList];
        if (shipping) {
            copiedFilterList.push("shipping")
        } else {
            copiedFilterList = copiedFilterList.filter((filter) => filter !== "shipping")
        }
        setFilterList(copiedFilterList)
    }

    function freeItemFilter() {
        let freeItemFlag = !filterObject.freeItem
        setFilterObject({ ...filterObject, freeItem: freeItemFlag })
        let copiedFilterList = [...filterList];
        if (freeItemFlag) {
            copiedFilterList.push("freeItem")
        } else {
            copiedFilterList = copiedFilterList.filter((filter) => filter !== "freeItem")
        }
        setFilterList(copiedFilterList)
    }

    function filterSelect(e) {
        setFilterObject({ ...filterObject, [e.target.name]: e.target.value })
        let copiedFilterList = [...filterList];
        if (e.target.value !== "") {
            copiedFilterList.push(e.target.name)
        } else {
            copiedFilterList = copiedFilterList.filter((filter) => filter !== e.target.name)
        }
        setFilterList(copiedFilterList);
    }

    function clearFilters() {
        setFilterObject({
            category: "",
            condition: "",
            contactMethod: "",
            location: "",
            paymentMethod: "",
            shipping: false,
            minPrice: "",
            maxPrice: "",
            sortBy: ""
        })
        setFilterList([]);
    }

    function sortByFunction(e) {
        setFilterObject({ ...filterObject, [e.target.name]: e.target.value })
    }

    return (
        <div>
            <Navbar className='forMobile' collapseOnSelect expand="lg" >
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Form id='itemfilterform'>
                        <Form.Check className="shipping-checkbox mb-3 filter-Options itemToggle"
                            type='switch'
                            label='Shipping Available'
                            onClick={() => shippingFilter()}
                        />
                        <Form.Check className="free-item-checkbox mb-3 filter-Options itemToggle"
                            type='switch'
                            label='Free Items'
                            onClick={() => freeItemFilter()}
                        />
                        <Form.Group className='filter-Options'>
                            <Form.Label>Category</Form.Label>
                            <Form.Select name="category" id='itemFilterCategory' value={filterObject.category} onChange={filterSelect}>
                                <option key="categoryInitialEmptyValue" value="">Choose Category</option>
                                {dropDown.category.length > 0 ? dropDown.category.map(cat => {
                                    return (
                                        <option key={cat.name} value={cat.name}>{cat.name}</option>
                                    )
                                }) : <option>Loading</option>}
                            </Form.Select>
                        </Form.Group>
                        <Form.Group className='filter-Options'>
                            <Form.Label>Location</Form.Label>
                            <Form.Select name="location" id='itemFilterLocation' value={filterObject.location} onChange={filterSelect}>
                                <option key="locationInitialEmptyValue" value="">Choose a Location</option>
                                {dropDown.location.length > 0 ? dropDown.location.map(loc => {
                                    return (
                                        <option key={loc.name} value={loc.name}>{loc.name}</option>
                                    )
                                }) : <option>Loading</option>}
                            </Form.Select>
                        </Form.Group>
                        <Form.Group className='filter-Options'>
                            <Form.Label>Preferred Contact Method</Form.Label>
                            <Form.Select name="contactMethod" id='itemFilterContactMethod' value={filterObject.contactMethod} onChange={filterSelect}>
                                <option key="contactMethodInitialEmptyValue" value="">Choose a Contact Method</option>
                                {dropDown.contactMethod.length > 0 ? dropDown.contactMethod.map(con => {
                                    return (
                                        <option key={con.name} value={con.name}>{con.name}</option>
                                    )
                                }) : <option>Loading</option>}
                            </Form.Select>
                        </Form.Group>
                        <Form.Group className='filter-Options'>
                            <Form.Label>Preferred Payment</Form.Label>
                            <Form.Select name="paymentMethod" id='itemFilterPaymentMethod' value={filterObject.paymentMethod} onChange={filterSelect}>
                                <option key="paymentMethodInitialEmptyValue" value="">Choose a Payment Method</option>
                                {dropDown.paymentMethod.length > 0 ? dropDown.paymentMethod.map(pay => {
                                    return (
                                        <option key={pay.name} value={pay.name}>{pay.name}</option>
                                    )
                                }) : <option>Loading</option>}
                            </Form.Select>
                        </Form.Group>
                        <Form.Group className='filter-Options'>
                            <Form.Label>Condition</Form.Label>
                            <Form.Select name="condition" id='itemFilterCondition' value={filterObject.condition} onChange={filterSelect}>
                                <option key="conditionInitialEmptyValue" value="">Choose a Condition</option>
                                {dropDown.condition.length > 0 ? dropDown.condition.map(cond => {
                                    return (
                                        <option key={cond.name} value={cond.name}>{cond.name}</option>
                                    )
                                }) : <option>Loading</option>}
                            </Form.Select>
                        </Form.Group>
                        <Form.Group className='filter-Options'>
                            <Form.Label>Price Filter</Form.Label>
                            <InputGroup className='min-max-filter'>
                                <InputGroup.Text>$</InputGroup.Text>
                                <FormControl name="minPrice" id='itemFilterMinPrice' type="number" min="0" placeholder="Minimum Price" value={filterObject.minPrice} onChange={sortByFunction} />
                            </InputGroup>
                            <InputGroup className='min-max-filter'>
                                <InputGroup.Text>$</InputGroup.Text>
                                <FormControl name="maxPrice" id='itemFilterMaxPrice' type="number" min="0" placeholder="Maximum Price" value={filterObject.maxPrice} onChange={sortByFunction} />
                            </InputGroup>
                        </Form.Group>
                        <Form.Group className='filter-Options'>
                            <Form.Label>Sort By:</Form.Label>
                            <Form.Select name="sortBy" id='itemSortBy' value={filterObject.sortByFunction} onChange={sortByFunction}>
                                <option key="SortByInitialEmptyValue" value="">Choose a Sort By:</option>
                                <option key="SortByPriceAsc" value="sortByPriceAsc">Sort By Price Lowest to Highest ($...$$$)</option>
                                <option key="SortByPriceDesc" value="sortByPriceDesc">Sort By Price Highest to Lowest ($$$...$)</option>
                                <option key="SortByHottestItems" value="sortByHottestItems">Hottest Items</option>
                            </Form.Select>
                        </Form.Group>
                        <Button variant="item_primary" type="reset" onClick={clearFilters}>Clear Filters</Button>
                    </Form>
                </Navbar.Collapse>
            </Navbar>
        </div >
    )
}

export default Filter