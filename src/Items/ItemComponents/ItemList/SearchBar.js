import React from 'react';
import { useNavigate } from 'react-router-dom';
import { Form } from 'react-bootstrap';

function SearchBar({ searchQuery, setSearchQuery }) {
    const navigate = useNavigate();
    function onSubmit(e) {
        e.preventDefault();
        navigate(`?s=${searchQuery}`);
    }

    return (
        <Form action="/" method="get" onSubmit={onSubmit} >
            <Form.Label htmlFor="item-header-search">
                <span className="visually-hidden">Search items</span>
            </Form.Label>
            <Form.Control
                value={searchQuery}
                onInput={e => setSearchQuery(e.target.value)}
                type="text"
                id="item-header-search"
                placeholder="Search items"
                name="s"
            />
        </Form>
    )
}

export default SearchBar;