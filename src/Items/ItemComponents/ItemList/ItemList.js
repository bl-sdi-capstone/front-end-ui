import React, { useEffect, useContext } from 'react'
import { Row, Col, Container, Modal } from 'react-bootstrap'
import './ItemListStyles.css'
import SearchBar from './SearchBar'
import { useState } from 'react'
import Filter from './Filter'
import ItemContext from '../../Contexts/ItemContext'
import StickyBox from "react-sticky-box"
import ItemCard from '../ItemCard/ItemCard'
import CondorCard from '../CondorCards/CondorCard'

function ItemList() {
    const [state, dispatch] = useContext(ItemContext);
    const { search } = window.location;
    const query = new URLSearchParams(search).get('s');
    const [searchQuery, setSearchQuery] = useState(query || '');
    const [showTeam, setShowTeam] = useState(false);
    const itemsURL = `http://${state.apiUrl}/api/items`;

    useEffect(() => {
        fetch(itemsURL).then((response) => {
            return response.json();
        }).then((data) => {
            dispatch({ type: 'setItemList', payload: data.itemList })
        })
    }, []);

    let selectedFilterObject = {
        category: "",
        condition: "",
        contactMethod: "",
        location: "",
        paymentMethod: "",
        shipping: false,
        freeItem: false,
        minPrice: "",
        maxPrice: "",
        sortBy: ""
    }

    const [filterObject, setFilterObject] = useState(selectedFilterObject)
    let selectedFilterList = [];
    const [filterList, setFilterList] = useState(selectedFilterList)

    const filterItems = (query) => {
        let filteredArray = [...state.itemList]

        if (query) {
            filteredArray = filteredArray.filter((item) => {
                const itemTitle = item.title.toLowerCase();
                return itemTitle.includes(query.toLowerCase());
            })
        }

        if (filterList.length > 0) {
            filteredArray = filteredArray.filter((item) => {
                let filtered = filterItem(item);
                return filtered;
            })
        }

        if (filterObject.sortBy === "sortByPriceAsc") {
            filteredArray.sort(function (a, b) { return a.price - b.price })
        }
        if (filterObject.sortBy === "sortByPriceDesc") {
            filteredArray.sort(function (a, b) { return b.price - a.price })
        }
        if (filterObject.sortBy === "sortByHottestItems") {
            filteredArray.sort(function (a, b) { return b.views - a.views })
        }
        if (filterObject.minPrice !== "") {
            filteredArray = filteredArray.filter((item) => {
                return item.price > filterObject.minPrice
            })
        }
        if (filterObject.maxPrice !== "") {
            filteredArray = filteredArray.filter((item) => {
                return item.price < filterObject.maxPrice
            })
        }

        if (searchQuery === "east bay andean condors") {
            setShowTeam(true)
            setSearchQuery("")
        }
        return filteredArray
    };

    const filterItem = (item) => {
        let filteredFlag = true;
        filterList.forEach((filter) => {
            let checkFilter = checkItemFilter(item, filter);
            if (!checkFilter) {
                filteredFlag = false;
            }
        })
        return filteredFlag
    }

    const checkItemFilter = (item, filter) => {
        if (filter === "category") {
            if (item.category === filterObject.category) {
                return true
            }
        } else if (filter === "location") {
            if (item.location === filterObject.location) {
                return true
            }
        } else if (filter === "contactMethod") {
            if (item.contactMethod === filterObject.contactMethod) {
                return true
            }
        } else if (filter === "condition") {
            if (item.condition === filterObject.condition) {
                return true
            }
        } else if (filter === "paymentMethod") {
            if (item.paymentMethod === filterObject.paymentMethod) {
                return true
            }
        } else if (filter === "shipping") {
            if (item.willingToShip === filterObject.shipping) {
                return true
            }
        } else if (filter === "freeItem") {
            if (item.price === 0) {
                return true
            }
        }
        return false
    }

    const filteredItems = filterItems(searchQuery);
    const teamHide = () => setShowTeam(false);

    return (
        <>
            <Container id='item-list-page-main-column' fluid >
                <Row id='item-list-page-row' >
                    <Col id='item-list-page-filter-column' sm={2}>
                        <StickyBox id='stickyToResponsive'>
                            <SearchBar
                                searchQuery={searchQuery}
                                setSearchQuery={setSearchQuery} />
                            <Filter
                                setFilterObject={setFilterObject}
                                filterObject={filterObject}
                                setFilterList={setFilterList}
                                filterList={filterList} />
                        </StickyBox>
                    </Col>
                    <Col sm={10}>
                        <Container>
                            <Row id='itemListTitleRow'>
                                <h1 id='item-list-page-list-header'>{filterObject.category === "" ? "All Items" : filterObject.category}</h1>
                                {filteredItems.length > 0 ? filteredItems.map(item => {
                                    return (
                                        <Col key={item.id + item.title}>
                                            <ItemCard item={item} key={item.id + item.title}></ItemCard>
                                        </Col>
                                    )
                                }) : "No items found"}
                            </Row>
                        </Container>
                    </Col>
                </Row>
            </Container>

            <Modal id="team-card-modal" show={showTeam} onHide={teamHide}>
                <CondorCard />
            </Modal>
        </>
    )
}

export default ItemList