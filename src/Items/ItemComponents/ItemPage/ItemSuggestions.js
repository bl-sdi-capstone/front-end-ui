import React, { useContext, useEffect } from 'react';
import { Row, Col, Container, Button } from 'react-bootstrap';
import ItemContext from '../../Contexts/ItemContext';
import ItemCard from '../ItemCard/ItemCard';

function ItemSuggestions({ pageItem }) {
  const [state, dispatch] = useContext(ItemContext);
  const itemsURL = `http://${state.apiUrl}/api/items`;

  useEffect(() => {
    fetch(itemsURL).then((response) => {
      return response.json();
    }).then((data) => {
      dispatch({ type: 'setItemList', payload: data.itemList })
    })
  }, []);

  const filterItems = () => {
    let filteredArray = [...state.itemList];

    filteredArray = filteredArray.filter((listItem) => {
      if (listItem.category === pageItem.category && listItem.id !== pageItem.id) {
        return listItem;
      }
    })
    return filteredArray;
  }

  const shipItem = (item) => {
    if (item.willingToShip === true) {
      return "Shipping Available";
    } else {
      return "Pick-up Only";
    }
  }

  const filteredItems = filterItems();

  if (filteredItems.length > 0) {
    return (
      <Container id='suggestionContainer'>
        <Col id='suggestionCol'>
          <Container>
            <Row id='suggestionRow'>
              {filteredItems.length === 1 ?
                <h4 id='suggestionHeader'>You May Also Like This</h4> :
                <h4 id='suggestionHeader'>Items You May Also Like</h4>
              }
              {filteredItems.length > 0 ? filteredItems.slice(0, 4).map(item => {
                return (
                  <Col className="suggestionListCol" key={item.id + item.title}>
                    <ItemCard item={item} key={item.id + item.title}></ItemCard>
                  </Col>
                )
              }) : "No items found"}
            </Row>
          </Container>
        </Col>
      </Container>
    )
  } else {
    return (<span></span>)
  }
}

export default ItemSuggestions