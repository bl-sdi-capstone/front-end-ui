import React, { useEffect, useState, useContext } from 'react';
import { Container, Col, Row, Button, Modal, Form, InputGroup, FormControl, Image, Card, Table, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { useParams, useNavigate } from 'react-router-dom';
import './ItemPageStyles.css'
import ItemContext from '../../Contexts/ItemContext';
import AuthContext from '../../../contexts/AuthContext';
import ItemSuggestions from './ItemSuggestions';
import { Accordion } from 'react-bootstrap';
import { FaShare, FaFlag, FaUserAlt } from "react-icons/fa";
import { TbZoomIn } from 'react-icons/tb'
import { ImLocation2 } from 'react-icons/im'
import { BsEyeFill } from 'react-icons/bs'
import { RiArrowGoBackFill } from "react-icons/ri"
import { MdEmail } from 'react-icons/md'
import { BsFillTelephoneFill } from 'react-icons/bs'
import { GiEgyptianBird } from 'react-icons/gi'

function ItemPage() {
    const [item, setItem] = useState({});
    const [show, setShow] = useState(false);
    const [currentEdit, setCurrentEdit] = useState(item);
    const [currentImage, setCurrentImage] = useState({});
    const [imageChange, setImageChange] = useState(false);
    const [validated, setValidated] = useState(false);
    const [textAreaCount, setTextAreaCount] = useState(0);
    const [titleTextCount, setTitleTextCount] = useState(0);
    const [shareTooltip, setShareToolTip] = useState(false);
    const [flagTooltip, setFlagToolTip] = useState(false);
    const [imageModal, setImageModal] = useState(false);
    const [phoneInputValue, setPhoneInputValue] = useState("");
    const [patchSent, setPatchSent] = useState(false);
    const [state, dispatch] = useContext(ItemContext);
    const [authState,] = useContext(AuthContext);
    const navigate = useNavigate();
    const dropDown = state.dropDownData;

    const handleClose = () => setShow(false);
    const handleShow = () => {
        getLatestItem()
        setShow(true)
    };

    let params = useParams();
    let singleItemURL = `http://${state.apiUrl}/api/items/${params.itemID}`
    let patchItemViewURL = `http://${state.apiUrl}/api/items/${params.itemID}/views`
    const dropdownUrl = `http://${state.apiUrl}/api/seeder`

    const onInputChange = (e) => {
        const form = e.currentTarget;

        if (form.checkValidity() === false) {
            e.preventDefault();
            e.stopPropagation();
        }
        setValidated(true);

        if (e.target.name === "description") {
            setTextAreaCount(e.target.value.length);
        }
        if (e.target.name === "title") {
            setTitleTextCount(e.target.value.length);
        }
        let value = e.target.value;
        if (e.target.name === "willingToShip" && e.target.value === "true") {
            value = true;
          } 
        if (e.target.name === "willingToShip" && e.target.value === "false") {
            value = false;
        }
        setCurrentEdit({ ...currentEdit, [e.target.name]: value });
    }

    const imageHandler = (e) => {
        setCurrentImage(e.target.files[0]);
        setImageChange(true);
    }

    const getLatestItem = () => {
        fetch(singleItemURL)
            .then((resp) => {
                return resp.json();
            })
            .then((data) => {
                let updatedData = data;
                setItem(updatedData);
                setCurrentEdit(updatedData)
                setPhoneInputValue(formatPhoneNumber(updatedData.phoneNumber))
            })
    }

    useEffect(() => {
        getLatestItem();
        window.scrollTo(0, 0)
    }, [params]);

    useEffect(() => {
        if (item.status !== "Inactive") {
            const viewOptions = {
                method: "PATCH",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify('')
            };
            fetch(patchItemViewURL, viewOptions).then((response) => {
                if (response.ok) {
                    return response.json();
                }
            }).then((newData) => {
                dispatch({ type: "edit", payload: newData })
                setItem(newData)
                setCurrentEdit(newData)
                window.scrollTo(0, 0)
            })
        }
    }, [])

    useEffect(() => {
        fetch(dropdownUrl).then((response) => {
            return response.json();
        }).then((data) => {
            dispatch({ type: 'setDropdownData', payload: data })
        })
    }, []);

    useEffect(() => {
        fetch(singleItemURL).then((response) => {
            return response.json();
        }).then((data) => {
            setItem(data);
        })
    }, [show]);

    const shipItem = (item) => {
        if (item.willingToShip === true) {
            return "Available";
        } else {
            return "Pick-up Only";
        }
    }

    function deleteItem() {
        const deleteOptions = {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': authState.token
            },
        };
        fetch(singleItemURL, deleteOptions).then((response) => {
            if (response.ok) {
                dispatch({ type: "delete", payload: item.id })
                navigate('/profile');
            }
        })
    }

    function saveEditedItem(e) {
        e.preventDefault();
        let name = e.nativeEvent.submitter.name;        
        let newCurrentEdit = { ...currentEdit };
        if (name === "publish" && currentEdit.status === "Inactive") {
            newCurrentEdit.status = "Active"
            setCurrentEdit(newCurrentEdit)
        }
        const form = e.currentTarget;
        if (form.checkValidity()) {
            const editOptions = {
                method: "PATCH",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authState.token
                },
                body: JSON.stringify(newCurrentEdit)
            };
            fetch(singleItemURL, editOptions).then((response) => {
                if (response.ok) {
                    const formData = new FormData();
                    formData.append('multipartImage', currentImage);
                    const imageURL = `http://${state.apiUrl}/api/items/${currentEdit.id}/image`
                    if (imageChange) {
                        fetch(imageURL, {
                            method: "POST",
                            headers: {
                                'Authorization': authState.token
                            },
                            body: formData,
                        }).then(response => {
                            setImageChange(false);
                            getLatestItem();
                        });
                    }
                }
            }).then(() => {
                handleClose();
                setPatchSent(false);
                getLatestItem();
            })
        }
    }

    function cancelEdit() {
        setCurrentEdit(item);
        handleClose();
    };

    const imageHide = () => setImageModal(false);

    function copyURL() {
        let currentURL = `http://yardsale.galvanizelaboratory.com/items/ItemPage/${params.itemID}`;
        var textarea = document.createElement("textarea");
        textarea.textContent = currentURL;
        textarea.style.position = "fixed";
        document.body.appendChild(textarea);
        textarea.select();
        try {
            return document.execCommand("copy");
        }
        catch (ex) {
            console.warn("Copy to clipboard failed.", ex);
            return prompt("Copy to clipboard: Ctrl+C, Enter", currentURL);
        }
        finally {
            setShareToolTip(true);
            document.body.removeChild(textarea);
            setTimeout(() => { setShareToolTip(false); }, 1000);
        }
    }

    function reportItem() {
        const editOptions = {
            method: "PATCH",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ ...currentEdit, violationFlagged: true })
        };
        fetch(singleItemURL + "/report", editOptions).then((response) => {
            if (response.ok) {
                setCurrentEdit({ ...currentEdit, violationFlagged: true })
                setItem({ ...currentEdit, violationFlagged: true })
                setFlagToolTip(true);
                setTimeout(() => { setFlagToolTip(false); }, 1000);
            }
        })
    }

    function daysListed(item) {
        if (item.status === "Active") {
            let posted = new Date(item.postedDate);
            let current = new Date();
            return (Math.ceil((current.getTime() - posted.getTime()) / (1000 * 3600 * 24)));
        } else {
            return "Draft"
        }
    }

    function imageShow() {
        setImageModal(!imageModal);
    }

    function navigateBack() {
        window.history.back();
    }

    function condor() {
        navigate("https://www.chimuadventures.com/blog/wp-content/uploads/2016/07/shutterstock_113994430.jpg")
    }

    const handlePhoneNumber = (e) => {
        // this is where we'll call the phoneNumberFormatter function
        const formattedPhoneNumber = formatPhoneNumber(e.target.value);
        // we'll set the input value using our setPhoneInputValue
        setPhoneInputValue(formattedPhoneNumber);

        // Update currentUser
        const phoneNumber = formattedPhoneNumber.replace(/[^\d]/g, "");
        setCurrentEdit({ ...currentEdit, [e.target.name]: phoneNumber });
    };

    function formatPhoneNumber(value) {
        // if input value is falsy eg if the user deletes the input, then just return
        if (!value) return value;

        // clean the input for any non-digit values.
        const phoneNumber = value.replace(/[^\d]/g, "");

        // phoneNumberLength is used to know when to apply our formatting for the phone number
        const phoneNumberLength = phoneNumber.length;

        // we need to return the value with no formatting if its less then four digits
        // this is to avoid weird behavior that occurs if you  format the area code to early
        if (phoneNumberLength < 4) return phoneNumber;

        // if phoneNumberLength is greater than 4 and less the 7 we start to return
        // the formatted number
        if (phoneNumberLength < 7) {
            return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(3)}`;
        }

        // finally, if the phoneNumberLength is greater then seven, we add the last
        // bit of formatting and return it.
        return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(3, 6)}-${phoneNumber.slice(6, 10)}`;
    }

    return (
        <>
            <Container id='itemPageWholeContainer'>
                <Row id='returnToItemListRow'>
                    <span id='returnToItemList' onClick={navigateBack}> <RiArrowGoBackFill /> Return to Previous Page</span>
                </Row>
                <Row>
                    <Col className='itemPageImage'>
                        <Container >
                            <Card key={item.id} id='itemPageTextCard'>
                                <Card.Body className='imageBody'>
                                    {item.picture === undefined ? "Picture Loading" : <Image fluid onClick={imageShow} id='itemPageImage' variant="top" src={`http://${state.apiUrl}/api/images/${item.picture}`} />}
                                    <p id='clickToZoom'><TbZoomIn />Click image to enlarge</p>
                                </Card.Body>
                            </Card>
                        </Container>
                    </Col>
                    <Col className='itemPageImageCol'>
                        <Container id='individualItemCardContainer'>
                            <Card key={item.id} id='itemPageTextCard'>
                                <Card.Body>
                                    {item.status === "Inactive" ? <h2 id='draftIndicator'>* DRAFT *</h2> : ""}
                                    <div id='itemViewAndShare'>
                                        <span id='viewsInfo'><BsEyeFill id='viewIcon' /> {item.views} </span>
                                        <div id='imagePageButtons'>
                                            <OverlayTrigger
                                                show={shareTooltip ? true : false}
                                                placement="bottom"
                                                overlay={(
                                                    <Tooltip>
                                                        Link Copied!
                                                    </Tooltip>
                                                )}>
                                                <span id='shareButton' onClick={copyURL}><FaShare id='shareIcon' /></span>
                                            </OverlayTrigger>

                                            <OverlayTrigger
                                                show={flagTooltip ? true : false}
                                                placement="bottom"
                                                overlay={(
                                                    <Tooltip>
                                                        Listing Reported!
                                                    </Tooltip>
                                                )}>
                                                <span id='flagButton' onClick={reportItem}><FaFlag id='flagIcon' /> </span>
                                            </OverlayTrigger>
                                        </div>
                                    </div>
                                    <Card.Title id='itemTitle'>{item.title}</Card.Title>
                                    <Card.Title id='itemPrice'>{item.price === 0 ? "Free" : `$${item.price}`}</Card.Title>

                                </Card.Body>
                                <Table className="table table-borderless" id='fullTable' size="sm">
                                    <tbody>
                                        <tr>
                                            <td className="tableItems"><span className='itemPageCardTitles'>Condition: </span>{item.condition}</td>
                                            <td className="tableItems"><span className='itemPageCardTitles'>Shipping: </span>{shipItem(item)}</td>
                                        </tr>
                                        <tr>
                                            <td className="tableItems"><span className='itemPageCardTitles'>Payment: </span>{item.paymentMethod} </td>
                                            <td className="tableItems"><span className='itemPageCardTitles'>Days Listed: </span>{daysListed(item)}</td>

                                        </tr>
                                    </tbody>
                                </Table>

                                <Card.Body>
                                    <span id='cardDescriptionText'>Description:</span>
                                    <Card.Text id='itemPageDescription'>{item.description}</Card.Text>
                                </Card.Body>

                                <Card.Body>
                                    <Card.Title> <FaUserAlt className='usernameAndLocationIcons' /> {item.username} | <ImLocation2 className='usernameAndLocationIcons' /> {item.location} </Card.Title>
                                    <Accordion>
                                        <Accordion.Item eventKey='0' >
                                            <Accordion.Header>Contact Information</Accordion.Header>
                                            <Accordion.Body className='contactInfoAcc'><span className='itemPageCardTitles'> <span className='contactIcon'><a id='condor' target='_blank' href='https://www.chimuadventures.com/blog/wp-content/uploads/2016/07/shutterstock_113994430.jpg'><GiEgyptianBird /></a></span>Preferred Contact Method: </span> {item.contactMethod}</Accordion.Body>
                                            <Accordion.Body className='contactInfoAcc'><span className='itemPageCardTitles'> <span className='contactIcon'><BsFillTelephoneFill /></span>Phone Number:  </span> <a className='emailAndPhone' href={`tel:${item.phoneNumber}`} obfuscate="true">{formatPhoneNumber(item.phoneNumber)}</a></Accordion.Body>
                                            <Accordion.Body className='contactInfoAcc'><span className='itemPageCardTitles'> <span className='contactIcon'><MdEmail /></span>Email Address: </span> <a className='emailAndPhone' href={`mailto:${item.emailAddress}`} obfuscate="true">{item.emailAddress}</a></Accordion.Body>
                                        </Accordion.Item>
                                    </Accordion>
                                </Card.Body>
                            </Card>

                            <div id='itemPageButtons'>
                                {item.username === authState.username ?
                                    <>
                                        <Button variant="item_primary" id='itemPageEditButton' onClick={handleShow}>Edit</Button>
                                        <Button variant="item_primary" id='itemPageDeleteButton' onClick={deleteItem}>Delete</Button>
                                    </>
                                    : <span></span>}
                            </div>
                        </Container>
                    </Col>
                </Row>
            </Container>

            <ItemSuggestions pageItem={item} />

            <Modal show={show} onHide={cancelEdit}>
                <Modal.Header closeButton onHide={cancelEdit}>
                    <Modal.Title>Edit Your Item</Modal.Title>
                </Modal.Header>

                <Form onSubmit={saveEditedItem} noValidate validated={validated} >
                    <Modal.Body>
                        <h1 id='item-main-header'>Item Information</h1>
                        <Form.Group>
                            <Row>
                                <Col>
                                    <Form.Label className='item-ind-title'>Title</Form.Label>
                                    <Form.Control required type="text" name="title" id='editItemTitle' maxLength="24" value={currentEdit.title} onChange={onInputChange}></Form.Control>
                                    <Form.Control.Feedback type="invalid" >Please enter a Title.</Form.Control.Feedback>
                                    <span className={titleTextCount > 19 ? "itemTitleCounterRed" : "itemTitleCounter"}>{titleTextCount}/24</span>
                                </Col >
                                <Col>
                                    <Form.Label className='item-ind-title'>Price</Form.Label>
                                    <InputGroup>
                                        <InputGroup.Text>$</InputGroup.Text>
                                        <FormControl name="price" id='editItemPrice' type="number" min="0" value={currentEdit.price} onChange={onInputChange} />
                                    </InputGroup>
                                </Col>
                            </Row >
                        </Form.Group >

                        <Form.Group>
                            <Row>
                                <Col>
                                    <Form.Label className='item-ind-title'>Category</Form.Label>
                                    <Form.Select name="category" id='editItemCategory' value={currentEdit.category} onChange={onInputChange}>
                                        {dropDown.category.length > 0 ? dropDown.category.map(cat => {
                                            return (
                                                <option key={cat.name} value={cat.name}>{cat.name}</option>
                                            )
                                        }) : <option>Loading</option>}
                                    </Form.Select>
                                </Col>
                                <Col>
                                    <Form.Label className='item-ind-title'>Condition</Form.Label>
                                    <Form.Select name="condition" id='editItemCondition' value={currentEdit.condition} onChange={onInputChange}>
                                        {dropDown.condition.length > 0 ? dropDown.condition.map(cond => {
                                            return (
                                                <option key={cond.name} value={cond.name}>{cond.name}</option>
                                            )
                                        }) : <option>Loading</option>}
                                    </Form.Select>
                                </Col>
                            </Row>
                        </Form.Group>

                        <Form.Group controlid="formFile" className="mb-3" >
                            <Row>
                                <Form.Label className='item-ind-title'>Upload Picture</Form.Label>
                                <Form.Control type="file" name="picture" onChange={imageHandler} />
                            </Row>
                            <Row>
                                <Form.Label className='item-ind-title'>Description</Form.Label>
                                <Form.Control required className='createItemDescription' name="description" controlid='createItemDescription' value={currentEdit.description} onChange={onInputChange} as="textarea" maxLength="255" placeholder="Please enter a description"></Form.Control>
                                <Form.Control.Feedback type="invalid" ></Form.Control.Feedback>
                                <span className={textAreaCount > 225 ? "itemDescriptionCounterRed" : "itemDescriptionCounter"}>{textAreaCount}/255</span>
                            </Row>
                        </Form.Group>

                        <Form.Group >
                            <Form.Label htmlFor="willingToShip" className='item-ind-title'>Choose an Exchange Option</Form.Label>
                            <Form.Check checked={currentEdit.willingToShip ? true : false} inline type="radio" value={"true"} name="willingToShip" id='editItemWillingToShip' label="Willing to ship" onChange={onInputChange}></Form.Check>
                            <Form.Check checked={currentEdit.willingToShip ? false : true} inline type="radio" value={"false"} name="willingToShip" id='editItemNotWillingToShip' label="Pickup Only" onChange={onInputChange}></Form.Check>
                        </Form.Group>

                        <h1 id='item-main-header'>Seller Information</h1>
                        <Form.Group>
                            <Row>
                                <Col>
                                    <Form.Label className='item-ind-title'>Preferred Payment</Form.Label>
                                    <Form.Select name="paymentMethod" id='editItemPaymentMethod' value={currentEdit.paymentMethod} onChange={onInputChange}>
                                        {dropDown.paymentMethod.length > 0 ? dropDown.paymentMethod.map(pay => {
                                            return (
                                                <option key={pay.name} value={pay.name}>{pay.name}</option>
                                            )
                                        }) : <option>Loading</option>}
                                    </Form.Select>
                                </Col>
                                <Col>
                                    <Form.Label className='item-ind-title'>Preferred Contact</Form.Label>
                                    <Form.Select name="contactMethod" id='editItemContactMethod' value={currentEdit.contactMethod} onChange={onInputChange}>
                                        {dropDown.contactMethod.length > 0 ? dropDown.contactMethod.map(con => {
                                            return (
                                                <option key={con.name} value={con.name}>{con.name}</option>
                                            )
                                        }) : <option>Loading</option>}
                                    </Form.Select>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Form.Label className='item-ind-title'>Phone</Form.Label>
                                    <Form.Control required pattern="\([0-9]{3}\) [0-9]{3}-[0-9]{4}"
                                        type="tel" name="phoneNumber" id='editItemPhoneNumber' value={phoneInputValue} onChange={(e) => handlePhoneNumber(e)}></Form.Control>
                                    <Form.Control.Feedback type="invalid" >Please enter a Phone Number.</Form.Control.Feedback>
                                </Col>
                                <Col>
                                    <Form.Label className='item-ind-title'>Email</Form.Label>
                                    <Form.Control type="email" name="emailAddress" id='editItemEmailAddress' value={currentEdit.emailAddress} onChange={onInputChange}></Form.Control>
                                    <Form.Control.Feedback type="invalid" >Please enter an Email.</Form.Control.Feedback>
                                </Col>
                            </Row>
                        </Form.Group>
                    </Modal.Body >

                    <Modal.Footer>
                        <Button variant="secondary" id='editItemCancelButton' onClick={cancelEdit}>
                            Cancel
                        </Button>
                        {currentEdit.status === "Inactive" ? <Button variant="primary" id='editItemSaveButton' name='saveDraft' type='submit'>
                            Save Draft
                        </Button> : ""}
                        <Button variant="item_primary" id='editItemSaveButton' name='publish' type='submit'>
                            {currentEdit.status === "Inactive" ? "Publish" : "Save Changes"}
                        </Button>
                    </Modal.Footer>
                </Form >
            </Modal>

            <Modal centered show={imageModal} onHide={imageHide} className="item-image-modal">
                <Image onClick={imageShow} id='imageModal' variant="top" src={`http://${state.apiUrl}/api/images/${item.picture}`} />
            </Modal>
        </>
    )
}

export default ItemPage;