import './ServiceApp.css';
import { Outlet } from "react-router-dom";
import ServiceContext from './Contexts/ServiceContext';
import React, { useReducer } from 'react';

const initialState = {
  serviceList: [],
  managePostsList: [],
  categoryCounts: [],
  alvLocations: [],
  alvPaymentMethods: [],
  alvCategories: [],
  alvContactMethods: [],
  alvPriceUnits: []
}

function reducer(state, action) {
  switch (action.type) {
    case 'addServiceList':
    case 'addNewPosts':
    case 'filterServiceList':  
    case 'sortServiceList':
      return { ...state, serviceList: action.payload };
    case 'setCategories':
      return { ...state, categoryCounts: action.payload };
    case 'addLocationAlvs':
      return { ...state, alvLocations: action.payload };
    case 'addPaymentAlvs':
      return { ...state, alvPaymentMethods: action.payload };
    case 'addCategoryAlvs':
      return { ...state, alvCategories: action.payload };
    case 'addContactMethodsAlvs':
      return { ...state, alvContactMethods: action.payload };
    case 'addPriceUnitAlvs':
      return { ...state, alvPriceUnits: action.payload };
    case 'flaggedService':
      return { ...state, userInfo: action.payload };
    case 'addManagePosts':
      return { ...state, managePostsList: action.payload };
    case 'deletePost':
      let updatedCategoryCounts = [...state.categoryCounts];
      let foundCount = updatedCategoryCounts.find(cat => cat.category === action.payload.category);
      let foundCountIndex = updatedCategoryCounts.indexOf(foundCount);
      let updatedCount = updatedCategoryCounts[foundCountIndex].count - 1;
      let newCount = {};
      if (updatedCount > 0) {
        newCount = { id: updatedCategoryCounts[foundCountIndex].id, category: updatedCategoryCounts[foundCountIndex].category, count: updatedCount };
        updatedCategoryCounts.splice(foundCountIndex, 1, newCount);
      } else {
        updatedCategoryCounts.splice(foundCountIndex, 1);
      }
      return { ...state, serviceList: [...state.serviceList.filter(service => { return service.id !== action.payload.id })], categoryCounts: updatedCategoryCounts };
    case 'deletePostFromManagePostsList':
      return { ...state, managePostsList: [...state.managePostsList.filter(service => { return service.id !== action.payload })] };
    case 'saveEditedPost':
      let updatedPosts = [...state.serviceList];
      let foundPost = updatedPosts.find(service => service.id === action.payload.id);
      let foundIndex = updatedPosts.indexOf(foundPost);
      updatedPosts.splice(foundIndex, 1, action.payload);
      return { ...state, serviceList: updatedPosts };
    default:
      return { ...state };
  }
}

function ServiceApp() {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <>
      <ServiceContext.Provider value={[state, dispatch]}>
        <Outlet />
      </ServiceContext.Provider>
    </>
  );
}

export default ServiceApp;
