import React, { useEffect, useState, useContext } from 'react'
import { Button, Form, Stack, InputGroup, Alert } from 'react-bootstrap';
import AuthContext from '../../contexts/AuthContext';
import UserContext from '../../contexts/UserContext';
import ServiceContext from '../Contexts/ServiceContext';
import { useNavigate, Link } from 'react-router-dom';

export default function ServiceForm({ path, handleShowDelete, serviceID }) {

  let defaultState = {
    title: "",
    description: "",
    location: "",
    category: "",
    priceValue: "",
    priceUnit: "",
    experienceYears: "",
    paymentMethods: [],
    contactMethod: "",
    emailAddress: "",
    phoneNumber: "",
    serviceRange: "",
    violationFlagged: false,
    activeStatus: false,
    lookingFor: false,
    availability: "",
  }

  const [authState, authDispatch] = useContext(AuthContext);
  const [userState, userDispatch] = useContext(UserContext);
  const [state, dispatch] = useContext(ServiceContext);
  const [postState, setPostState] = useState(defaultState);
  const [editMade, setEditMade] = useState(false);
  const [authExpMessage, setAuthExpMessage] = useState(false);

  const [validated, setValidated] = useState(false);
  const [validationPassed, setValidationPassed] = useState(false);
  const navigate = useNavigate();

  let singleServiceURL = `http://a464156a8a83a4311b2cde5c64247f4c-623028763.us-west-2.elb.amazonaws.com/api/services/${serviceID}`;
  let allowedValuesEndpoint = "http://a464156a8a83a4311b2cde5c64247f4c-623028763.us-west-2.elb.amazonaws.com/api/allowedvalues";
  let url = "http://a464156a8a83a4311b2cde5c64247f4c-623028763.us-west-2.elb.amazonaws.com/api/services";
  let addPostUrl = "http://a464156a8a83a4311b2cde5c64247f4c-623028763.us-west-2.elb.amazonaws.com/api/services";

  const getAlvs = () => {
    const options = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': authState.token
      }
    }
    fetch(allowedValuesEndpoint)
      .then((resp) => {
        return resp.json();
      })
      .then((data) => {

        dispatch({ type: "addLocationAlvs", payload: data.locations })
        dispatch({ type: "addPaymentAlvs", payload: data.paymentMethods })
        dispatch({ type: "addCategoryAlvs", payload: data.categories })
        dispatch({ type: "addContactMethodsAlvs", payload: data.contactMethods })
        dispatch({ type: "addPriceUnitAlvs", payload: data.priceUnits })
      })
  }

  useEffect(() => {
    if (path === '/services/AddNewPost') {
      if (authState.username !== null && authState.username !== "" && authState.username !== undefined) {
        setPostState({ ...postState, phoneNumber: userState.userInfo.phone ?? "", location: userState.userInfo.hub ?? "", emailAddress: userState.userInfo.email ?? ""});
      };
    } else {
      fetch(singleServiceURL)
        .then((resp) => {
          return resp.json();
        })
        .then((data) => {
          if (data.paymentMethods === undefined) {
            data.paymentMethods = [];
          }
          if (data.phoneNumber === undefined) {
            data.phoneNumber = "";
          }
          setPostState(data);
        })
    }
    if (state.alvLocations.length === 0) {
      getAlvs();
    }
  }, [userState])

  useEffect(() => {
    if (validationPassed) {
      path === '/services/AddNewPost' ? addNewPost() : saveEditedPost();
    }
  }, [validationPassed])

  const handleISOChange = (e) => {
    if (e.target.checked) {
      setPostState({ ...postState, lookingFor: true })
    } else {
      setPostState({ ...postState, lookingFor: false })
    }
    setEditMade(true);
  }

  const handleFormChange = (e) => {
    setEditMade(true);
    if (e.target.name === 'priceValue' || e.target.name === 'experienceYears' || e.target.name === 'serviceRange') {
      let inputNumber = formatNumberInputs(e.target.value);
      if (inputNumber !== "") {
        inputNumber = parseInt(e.target.value);
      } else {
        inputNumber = "";
      }
      setPostState({ ...postState, [e.target.name]: inputNumber })
    } else {
      setPostState({ ...postState, [e.target.name]: e.target.value })
    }

  }

  const handlePaymentMethodChange = (e) => {
    e.preventDefault();
    let selections = Array.from(e.target.selectedOptions, option => option.value)
    setEditMade(true);
    setPostState({ ...postState, paymentMethods: selections });
  }

  const handleClose = () => {
    setValidationPassed(false);
    setValidated(false);
    navigate(-1);
  };

  const handleProceed = (e) => {
    e.preventDefault();
    //all 3
    if (postState.priceValue === "" && postState.experienceYears === "" && postState.serviceRange === "") {
      setPostState({ ...postState, priceValue: 0, experienceYears: 0, serviceRange: 0 });
    }
    // price and exp
    if (postState.priceValue === "" && postState.experienceYears === "" && postState.serviceRange !== "") {
      setPostState({ ...postState, priceValue: 0, experienceYears: 0 });
    }
    // price and service
    if (postState.priceValue === "" && postState.experienceYears !== "" && postState.serviceRange === "") {
      setPostState({ ...postState, priceValue: 0, serviceRange: 0 });
    }
    // exp and service
    if (postState.priceValue !== "" && postState.experienceYears === "" && postState.serviceRange === "") {
      setPostState({ ...postState, experienceYears: 0, serviceRange: 0 });
    }
    //price only
    if (postState.priceValue === "" && postState.experienceYears !== "" && postState.serviceRange !== "") {
      setPostState({ ...postState, priceValue: 0 });
    }
    //exp only
    if (postState.priceValue !== "" && postState.experienceYears === "" && postState.serviceRange !== "") {
      setPostState({ ...postState, experienceYears: 0 });
    }
    //service only
    if (postState.priceValue !== "" && postState.experienceYears !== "" && postState.serviceRange === "") {
      setPostState({ ...postState, serviceRange: 0 });
    }
    handleSubmit(e);
  }

  const handleSubmit = (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();

    } else {
      if (event.currentTarget.name === 'saveForLater') {
        if (postState.activeStatus !== false) {
          setPostState({ ...postState, activeStatus: false });
          setEditMade(true);
        }
      } else {
        if (postState.activeStatus !== true) {
          setPostState({ ...postState, activeStatus: true });
          setEditMade(true);
        }
      }
      const cleanPhoneNumberLength = postState.phoneNumber.replace(/[^\d]/g, "").length;
      if (postState.title !== "" && postState.description !== "" && postState.location !== "" && postState.category !== ""
        && (postState.priceValue >= 0 || postState.priceValue === undefined) && (postState.experienceYears >= 0 || postState.experienceYears === undefined)
        && (postState.serviceRange >= 0 || postState.serviceRange === undefined) && (cleanPhoneNumberLength === 0 || cleanPhoneNumberLength === 10)) {
        setValidationPassed(true);
      }
    }
    setValidated(true);
  };

  const addNewPost = () => {
    const postToAdd = postState;

    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': authState.token
      },
      body: JSON.stringify(postToAdd)
    };
    fetch(`${addPostUrl}`, options)
      .then(response => {
        if (response.status === 401) {
          throw new Error();
        }
      })
      .then(() => { handleClose() })
      .catch((error) => {
        setAuthExpMessage(true);
      });
  }

  const saveEditedPost = () => {
    if (editMade) {
      const options = {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': authState.token
        },
        body: JSON.stringify(postState)
      };
      fetch(`${url}/${serviceID}`, options).then(response => {
        if (response.status === 401) {
          throw new Error();
        }
        if (response.ok) {
          dispatch({ type: 'saveEditedPost', payload: postState })
        }
      })
        .then(() => { handleClose() })
        .catch((error) => {
          setAuthExpMessage(true);
        });
    } else {
      handleClose();
    }
  }

  const formatPhoneNum = (inputPhoneNum) => {
    if (inputPhoneNum === "" || inputPhoneNum === undefined) {
      inputPhoneNum = "";
      return inputPhoneNum;
    }
    const cleanPhoneNumber = inputPhoneNum.replace(/[^\d]/g, "");
    const match = cleanPhoneNumber.match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
    let formattedPhoneNumber = !match[2] ? match[1] : '(' + match[1] + ') ' + match[2] + (match[3] ? '-' + match[3] : '');
    return formattedPhoneNumber;
  }

  const formatNumberInputs = (inputNumber) => {
    if (inputNumber === null || isNaN(inputNumber) === true || inputNumber === undefined || inputNumber === "") {
      let formattedNumber = "";
      return formattedNumber;
    } else {
      const cleanNumber = inputNumber.toString().replace(/[^\d]/g, "");
      return cleanNumber;
    }
  }

  const renderAuthExpMessage = () => {
    return (
    <Alert variant="danger" onClose={() => setAuthExpMessage(false)} dismissible>
        <Alert.Heading>Uh-oh, your session has timed out!</Alert.Heading>
        <p>
          You have been logged out of your account and your changes have not been saved.  Please log back in and try again.
        </p>
        <Link className='alert-link' to="/login">Login Here</Link>
      </Alert>
    )
  }

  return (
    <>
      {authExpMessage ? renderAuthExpMessage() : ""}
      <Form noValidate validated={validated} className="newPostForm">
        <Form.Label className='formHeader'>{path === '/services/AddNewPost' ? `Post New Service` : `Edit: ${postState.title} (${postState.activeStatus ? "Published" : "DRAFT"})`}</Form.Label>
        <br />
        <Form.Check type="checkbox" label="In Search Of" id='ISOcheck' onChange={handleISOChange} name='lookingFor' checked={postState.lookingFor ?? false} />
        <Form.Group>
          <Form.Label className="fw-bold" id="newPostLabel">*Title:</Form.Label>
          <Form.Control required size="sm" onChange={handleFormChange} name='title' type="text" placeholder="Title" className='inputField' id="titleDesc" value={postState.title ?? ""} />
          <Form.Control.Feedback type="invalid">Please enter a title</Form.Control.Feedback>
        </Form.Group>
        <Form.Group>
          <Form.Label className="fw-bold" id="newPostLabel">*Description:</Form.Label>
          <Form.Control required size="sm" onChange={handleFormChange} name='description' as="textarea" rows={3} value={postState.description ?? ""} className='inputField' id="descDetail" />
          <Form.Control.Feedback type="invalid">Please enter a description</Form.Control.Feedback>
        </Form.Group>
        <Stack direction="horizontal" gap={1}>
          <Stack>
            <Form.Label className="fw-bold" id="newPostLabel">*Category:</Form.Label>
            <Form.Select required size="sm" onChange={handleFormChange} name='category' id="catOption" className='inputField' value={postState.category}>
              <option value="">Select the category</option>
              {state.alvCategories.map((cat, i) => {
                return <option key={i} value={cat.name}>{cat.name}</option>;
              })}
            </Form.Select>
            <Form.Control.Feedback type="invalid">Please select a category</Form.Control.Feedback>
          </Stack>
          <Stack>
            <Form.Label className="fw-bold" id="newPostLabel">*Location:</Form.Label>
            <Form.Select required size="sm" onChange={handleFormChange} name='location' id="locOption" value={postState.location}>
              <option value="">Select your location</option>
              {state.alvLocations.map((loc, i) => {
                return <option key={i} value={loc.name}>{loc.name}</option>;
              })}
            </Form.Select>
            <Form.Control.Feedback type="invalid">Please select a location</Form.Control.Feedback>
          </Stack>
        </Stack>
        <Form.Group>
          <Form.Label className="fw-bold" id="newPostLabel">Price / Unit:</Form.Label>
          <Stack direction="horizontal" gap={1} className='inputField'>
            <InputGroup size="sm" className="mb-1">
              <InputGroup.Text>$</InputGroup.Text>
              <Form.Control id="price" size="sm" aria-label="Amount (to the nearest dollar)" onChange={handleFormChange} name='priceValue' type="text" value={(postState.priceValue ?? "0").toString()} />
              <InputGroup.Text>.00</InputGroup.Text>
            </InputGroup>
            <Form.Select size="sm" id="unit" className="mb-1" onChange={handleFormChange} name='priceUnit' value={postState.priceUnit}>
              <option value="">Select the price unit</option>
              {state.alvPriceUnits.map((price, i) => {
                return <option key={i} value={price.name}>{price.name}</option>;
              })}
            </Form.Select>
          </Stack>
        </Form.Group>
        <Form.Label className="fw-bold" id="newPostLabel">Availability:</Form.Label>
        <Form.Control id="availability" size="sm" onChange={handleFormChange} name='availability' as="textarea" placeholder="Enter your availability, e.g. M, W, F: 8am - 5pm Central" rows={3} className='inputField' value={postState.availability ?? ""} />
        <Form.Group>
          <Form.Label className="fw-bold" id="newPostLabel">Years of Experience:</Form.Label>
          <Form.Control id="yrsOfExp" size="sm" onChange={handleFormChange} name='experienceYears' type="text" className='inputField' value={(postState.experienceYears ?? "0").toString()} />
        </Form.Group>
        <Form.Group>
          <Form.Label className="fw-bold" id="newPostLabel">Service Range (miles):</Form.Label>
          <Form.Control id="serviceRange" size="sm" onChange={handleFormChange} name='serviceRange' type="text" className='inputField' value={(postState.serviceRange ?? "0").toString()} />
        </Form.Group>
        <Form.Label className="fw-bold" id="newPostLabel">Payment Methods:</Form.Label>
        <Form.Select id="pmtMethod" size="sm" multiple onChange={handlePaymentMethodChange} className='inputField' value={postState.paymentMethods}>
          {state.alvPaymentMethods.map((pmt, i) => {
            return <option key={i} value={pmt.name}>{pmt.name}</option>;
          })}
        </Form.Select>
        <Form.Label className="fw-bold" id="newPostLabel">Preferred Contact Method:</Form.Label>
        <Form.Select size="sm" onChange={handleFormChange} name='contactMethod' className='inputField' id="contactMethod" value={postState.contactMethod ?? ""}>
          <option value="">Select your preferred contact method</option>
          {state.alvContactMethods.map((contact, i) => {
            return <option key={i} value={contact.name}>{contact.name}</option>;
          })}
        </Form.Select>
        <Form.Label className="fw-bold" id="newPostLabel">Email Address:</Form.Label>
        <Form.Control id="email" size="sm" type="email" value={postState.emailAddress} placeholder="name@example.com" onChange={handleFormChange} name='emailAddress' className='inputField' />
        <Form.Group>
          <Form.Label className="fw-bold" id="newPostLabel">Phone Number:</Form.Label>
          <Form.Control id="phone" size="sm" type="tel" value={formatPhoneNum(postState.phoneNumber)} placeholder="(000) 000-0000" isInvalid={postState.phoneNumber.length === 0 ? "" : postState.phoneNumber.replace(/[^\d]/g, "").length === 10 ? "" : "true"} onChange={handleFormChange} name='phoneNumber' className='inputField' />
          <Form.Control.Feedback type="invalid">Phone number must include area code</Form.Control.Feedback>
        </Form.Group>
        <Stack direction='horizontal' gap={1} id="newPostButtons">
          {path === '/services/AddNewPost' ? "" : <Button variant='outline-danger' onClick={handleShowDelete} id="deleteButton">Delete</Button>}
          <Button variant='secondary' onClick={handleClose}>Cancel</Button>
          <Button name="saveForLater" variant='info' onClick={handleProceed}>Save For Later</Button>
          <Button name="publish" variant='primary' onClick={handleProceed} id="publish" >Publish</Button>
        </Stack>
      </Form>
    </>
  )
};
