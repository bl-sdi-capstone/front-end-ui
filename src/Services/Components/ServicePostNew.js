import React from 'react';
import ServiceForm from '../Components/ServiceForm';

export default function ServicePostNew() {

    return (
        <div className='background-newPost'>
            <ServiceForm path='/services/AddNewPost'></ServiceForm>
        </div>
    )
};
