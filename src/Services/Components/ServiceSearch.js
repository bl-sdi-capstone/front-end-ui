import React, { useState, useContext, useEffect, useCallback } from "react";
import ServiceContext from "../Contexts/ServiceContext";
import { Form, Stack, Button, InputGroup, Accordion} from "react-bootstrap";
import ServiceCategoryItem from "./ServiceCategoryItem";
import AuthContext from "../../contexts/AuthContext";
import UserContext from "../../contexts/UserContext";
import FilterContext from "../Contexts/FilterContext";
import { useNavigate } from "react-router-dom";
import StickyBox from "react-sticky-box";
import "../ServiceApp.css";
import debounce from "lodash.debounce";
import Sliders from "./Sliders";

export default function ServiceSearch() {
    const [state, dispatch] = useContext(ServiceContext);
    const [authState] = useContext(AuthContext);
    const [userState] = useContext(UserContext);
    const [activeFilter, setActiveFilter, , setPriceSelect, , setDistanceSelect, , setYoeSelect] = useContext(FilterContext);
    const [location, setLocation] = useState("");
    const { search } = window.location;
    const query = new URLSearchParams(search).get("s");
    const navigate = useNavigate();
    const [keyword, setKeyWord] = useState(query || "");
    const isEmpty = keyword === "" ?? false;


    let baseUrl =
        "http://a464156a8a83a4311b2cde5c64247f4c-623028763.us-west-2.elb.amazonaws.com/api/services";
    let categoryEndpoint = `http://a464156a8a83a4311b2cde5c64247f4c-623028763.us-west-2.elb.amazonaws.com/api/services/counts?location=${location}`;
    let allowedValuesEndpoint =
        "http://a464156a8a83a4311b2cde5c64247f4c-623028763.us-west-2.elb.amazonaws.com/api/allowedvalues";
    let allServicesEndpoint =
        "http://a464156a8a83a4311b2cde5c64247f4c-623028763.us-west-2.elb.amazonaws.com/api/services";

    useEffect(() => {
        getAlvs();
        if (query !== "" && query !== null) {
            setLocation("All");
        }
    }, []);

    useEffect(() => {
        if (Object.keys(userState.userInfo).length > 0) {
            if (query === null || query === "") {
                setLocation(userState.userInfo.hub);
            }
        } else if (authState.username === "" || authState.username === undefined) {
            setLocation("All");
        }
    }, [userState.userInfo]);

    useEffect(() => {
        if (location !== "") {
            getCategories();
        }
        if ((query === null || query === "") && location !== "") {
            getAllActiveResults();
        }
    }, [location]);

    useEffect(() => {
        if (query !== null && query !== "" && keyword !== "") {
            fetchByKeyword();
        } else {
            if (location !== "") {
                getAllActiveResults();
                let userLocation = userState.userInfo.hub ?? "All";
                setLocation(userLocation);
                navigate("/services");
            }
        }
    }, [query]);

    const handleLocationChange = (e) => {
        setLocation(e.target.value);
        setYoeSelect(0);
        setDistanceSelect(0);
        setPriceSelect(0);
        setActiveFilter(false);
    };

    const debouncedFilter = useCallback(
        debounce((e) => {
            if (location !== "All") {
                setLocation("All");
            }
            setYoeSelect(0);
            setDistanceSelect(0);
            setPriceSelect(0);
            setActiveFilter(false); 
            navigate(`?s=${e.target.value}`);
        }, 500),
        []
    );

    const handleKeywordChange = (e) => {
        setKeyWord(e.target.value);
        debouncedFilter(e);
    };

    const resetSearchBox = () => {
        navigate("/services");
        setKeyWord("");
    };

    const getAlvs = () => {
        fetch(allowedValuesEndpoint)
            .then((resp) => {
                return resp.json();
            })
            .then((data) => {
                dispatch({ type: "addLocationAlvs", payload: data.locations });
                dispatch({ type: "addPaymentAlvs", payload: data.paymentMethods });
                dispatch({ type: "addCategoryAlvs", payload: data.categories });
                dispatch({
                    type: "addContactMethodsAlvs",
                    payload: data.contactMethods,
                });
                dispatch({ type: "addPriceUnitAlvs", payload: data.priceUnits });
            });
    };

    const getCategories = () => {
        let url =
            "http://a464156a8a83a4311b2cde5c64247f4c-623028763.us-west-2.elb.amazonaws.com/api/services/counts";
        if (location !== "All" && location !== "" && location !== "Remote") {
            url = categoryEndpoint;
        }
        fetch(url)
            .then((resp) => {
                if (resp.status === 204) {
                    throw new Error();
                } else {
                    return resp.json();
                }
            })
            .then((data) => {
                let catsWithId = data.categories.map((category, i) => {
                    return {
                        id: (i += 1),
                        category: category.category,
                        count: category.count,
                    };
                });
                catsWithId.sort((a, b) =>
                    a.category.toLowerCase() > b.category.toLowerCase() ? 1 : -1
                );
                dispatch({ type: "setCategories", payload: catsWithId });
            })
            .catch((error) => {
                dispatch({ type: "setCategories", payload: [] });
            });
    };

    const fetchByKeyword = () => {
        fetch(`${baseUrl}/search?word=${keyword}`)
            .then((resp) => {
                if (resp.status === 204) {
                    throw new Error();
                } else {
                    return resp.json();
                }
            })
            .then((data) => {
                let serviceListActive = data.services.filter(
                    (service) => service.activeStatus === true
                );
                dispatch({ type: "addServiceList", payload: serviceListActive });
            })
            .catch((error) => {
                dispatch({ type: "addServiceList", payload: [] });
            });
    };

    const onSubmit = (e) => {
        e.preventDefault();
        navigate(`?s=${keyword}`);
    };

    const getAllActiveResults = () => {
        let baseUrl = "";
        if (location !== "All" && location !== "" && location !== "Remote") {
            baseUrl = `${allServicesEndpoint}?location=${location}`;
        } else {
            baseUrl = `${allServicesEndpoint}`;
        }

        fetch(baseUrl)
            .then((resp) => {
                if (resp.status === 204) {
                    throw new Error();
                } else {
                    return resp.json();
                }
            })
            .then((data) => {
                let serviceListActive = data.services.filter(
                    (service) => service.activeStatus === true
                );
                dispatch({ type: "addServiceList", payload: serviceListActive });
            })
            .catch((error) => {
                dispatch({ type: "addServiceList", payload: [] });
            });
    };

    const handleAddNewClick = (e) => {
        e.preventDefault();
        if (
            authState.username !== "" &&
            authState.username !== null &&
            authState.username !== undefined
        ) {
            navigate("/services/AddNewPost");
        } else {
            alert("must be logged in to create a new post");
        }
    };

    const resetAll = () => {
        setPriceSelect(0);
        setDistanceSelect(0);
        setYoeSelect(0);
        getAllActiveResults();
        getCategories();
        setActiveFilter(false);
    }

    const generateCategories = () => {
        let categories = state.categoryCounts;
        if (categories.length === 0) {
            if (activeFilter) {
                return `There are no services that match your filter selections.`;
            }
            return `There are no services currently offered for ${location}.`;
        }
        let counts = state.categoryCounts.map((category) => {
            return (
                <ServiceCategoryItem
                    key={category.id}
                    category={category}
                    location={location}
                    activeFilter={activeFilter}
                />
            );
        });
        return counts;
    };

    return (
        <StickyBox>
            <Form onSubmit={onSubmit} className="searchBarTop">
                <Form.Group className="newPostButton">
                    <button className="App-link-button" onClick={handleAddNewClick}>
                        Create new post
                    </button>
                </Form.Group>
            </Form>
            <Form>
                <Accordion defaultActiveKey={isEmpty ? "1" : "0"} id="accordionSearchBar">
                    <Accordion.Item eventKey="0">
                        <Accordion.Header id="keywordClick">Keyword Search</Accordion.Header>
                        <Accordion.Body>
                            <Form.Group className="mb-1">
                                <Stack direction="vertical" gap={3}>
                                    <InputGroup className="searchbox">
                                        <Form.Control placeholder="Type to Search..." aria-label="Recipient's username" aria-describedby="basic-addon2" value={keyword} name="s" onChange={handleKeywordChange} />
                                    </InputGroup>
                                </Stack>
                            </Form.Group>
                        </Accordion.Body>
                    </Accordion.Item>

                    <Accordion.Item eventKey="1" onClick={() => resetSearchBox()}>
                        <Accordion.Header>Filters</Accordion.Header>
                        <Accordion.Body>
                            <Form.Group>
                                <Stack direction="vertical">
                                    <Form.Label label="Select a location:" className="fw-bold" id="locSelect">Select a Location: </Form.Label>
                                    <Form.Select onChange={handleLocationChange} value={location} id="locDropdown">
                                        <option value="All">All</option>
                                        {state.alvLocations.map((loc, i) => {
                                            return (
                                                <option key={i} value={loc.name}>
                                                    {loc.name}
                                                </option>
                                            );
                                        })}
                                    </Form.Select>
                                    {(activeFilter) ? <div className="filter-message">Filters Applied</div> : ""}
                                    <Form.Label className="fw-bold" id="catSelect">Select a Category:</Form.Label>
                                    <div className="selectedCat">{generateCategories()}</div>
                                </Stack>
                            </Form.Group>
                            <Form.Group>
                            </Form.Group>
                            <Sliders/>
                            <Stack direction="horizontal" gap={1} className="resetAllButton">
                                        <Button id="resetCurrentLocation" variant="primary" sx={{ backgroundColor: "#26c6da", margin: "10px" }} onClick={resetAll} disabled={!activeFilter}>Reset All</Button>
                            </Stack>
                        </Accordion.Body>
                    </Accordion.Item>
                </Accordion>
            </Form>
        </StickyBox>
    )
};
