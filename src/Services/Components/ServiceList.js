import React, { useContext } from "react";
import ServiceContext from "../Contexts/ServiceContext";
import FilterContext from "../Contexts/FilterContext";
import { Form, Stack, Button} from "react-bootstrap";
import Service from "./Service";
import '../ServiceApp.css';


export default function ServiceList() {
    const [state, dispatch] = useContext(ServiceContext);
    const [activeFilter, setActiveFilter] = useContext(FilterContext);

    const generateServices = () => {
        let services = state.serviceList;
        if (services.length === 0) {
            {if (activeFilter) {
                return 'There are no services that match your filter selections.';
            }}
            return `We couldn't find any services currently offered for the selected location. Please select other location or search by keyword.`
        }
        let newServiceList = state.serviceList.map(service => {
            return (<Service key={service.id} service={service} />);
        })
        return newServiceList;
    }

const determineHeader = () => {
    if (activeFilter){
        return <h1 className="headerTitle">Filtered Services</h1>
    } else {
        return <h1 className="headerTitle">Services</h1>
    }
}

let serviceListCopy = [...state.serviceList];

const sortAsc = () => {
    serviceListCopy.sort((a, b) =>
        a.title.toLowerCase() > b.title.toLowerCase() ? 1 : -1
    );
    dispatch({ type: "sortServiceList", payload: serviceListCopy });
};

const sortDsc = () => {
    serviceListCopy.sort((a, b) =>
        a.title.toLowerCase() > b.title.toLowerCase() ? -1 : 1
    );
    dispatch({ type: "sortServiceList", payload: serviceListCopy });
};

const sortUndo = () => {
    serviceListCopy.sort((a, b) =>
        a.postedDate < b.postedDate ? 1 : b.postedDate < a.postedDate ? -1 : 0
    );
    dispatch({ type: "sortServiceList", payload: serviceListCopy });
};


    return (
        <>  
            {determineHeader()}
            <Form>
            <Form.Label className="fw-bold">Sort by:</Form.Label>
                                <Stack direction="horizontal" gap={1} className="sortButtons">
                                    <Button variant="primary" sx={{ backgroundColor: "#26c6da", margin: "10px" }} onClick={sortAsc} className="AZButton">A-Z</Button>
                                    <Button variant="primary" sx={{ backgroundColor: "#26c6da", margin: "10px" }} onClick={sortDsc} className="ZAButton">Z-A</Button>
                                    <Button variant="primary" sx={{ backgroundColor: "#26c6da", margin: "10px" }} onClick={sortUndo} className="UndoButton">Undo</Button>
                                </Stack>
                                </Form>
            <div className="serviceList">
                {generateServices()}
            </div>
        </>
    )
};
