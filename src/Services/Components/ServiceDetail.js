import React from "react";
import { Card, Stack } from "react-bootstrap";

export default function ServiceDetail({ service }) {
  let pmtMethodString = service.paymentMethods ? service.paymentMethods.join(", ") : "";

  return (
    <Stack className="me-auto serviceStack">
      <Stack direction="horizontal" gap={1} className="me-auto mb-1">
        <Card.Text className="mb-1" id="cardText" style={{ fontWeight: 'bold' }}>Location:</Card.Text>
        <Card.Text className="mb-1 locationDetail">{service.location}</Card.Text>
      </Stack>
      <Stack direction="horizontal" gap={1} className="me-auto mb-1">
        <Card.Text className="mb-1" style={{ fontWeight: 'bold' }}>Service Range:</Card.Text>
        <Card.Text className="mb-1 serviceRangeDetail">{service.serviceRange} miles</Card.Text>
      </Stack>
      <Stack direction="horizontal" gap={1} className="me-auto mb-1">
        <Card.Text className="mb-1" style={{ fontWeight: 'bold' }}>Price:</Card.Text>
        <Card.Text className="mb-1 priceDetail">${service.priceValue} {service.priceUnit}</Card.Text>
      </Stack>
      <Stack className="me-auto mb-1">
        <Card.Text className="mb-0 me-auto" style={{ fontWeight: 'bold' }}>Availability:</Card.Text>
        <Card.Text className="mt-0 mb-1 me-auto availabilityDetail">{service.availability}</Card.Text>
      </Stack>
      <Stack direction="horizontal" gap={1} className="me-auto mb-1">
        <Card.Text className="mb-1" style={{ fontWeight: 'bold' }}>Years of Experience:</Card.Text>
        <Card.Text className="mb-1 yrsOfExpDetail">{service.experienceYears}</Card.Text>
      </Stack>
      <Stack className="me-auto mb-1">
        <Card.Text className="mb-0 me-auto" style={{ fontWeight: 'bold' }}>Payment Methods Accepted:</Card.Text>
        <Card.Text className="mb-1 me-auto pmtMethodDetail">{pmtMethodString}</Card.Text>
      </Stack>
      <Stack direction="horizontal" gap={1} className="me-auto mb-1">
        <Card.Text className="mb-1" style={{ fontWeight: 'bold' }}>Preferred Contact Method:</Card.Text>
        <Card.Text className="mb-1 preferredContactMethodDetail">{service.contactMethod}</Card.Text>
      </Stack>
      <Stack direction="horizontal" gap={1} className="me-auto mb-1">
        <Card.Text className="mb-1" style={{ fontWeight: 'bold' }}>Email Address:</Card.Text>
        <Card.Text className="mb-1 emailDetail">{service.emailAddress}</Card.Text>
      </Stack>
      <Stack direction="horizontal" gap={1} className="me-auto">
        <Card.Text className="mb-1" style={{ fontWeight: 'bold' }}>Phone Number:</Card.Text>
        <Card.Text className="mb-1 phoneDetail">{service.phoneNumber} </Card.Text>
      </Stack>
    </Stack>
  )
};
