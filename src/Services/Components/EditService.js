import { Modal, Button, Alert } from 'react-bootstrap';
import React, { useContext, useState } from 'react';
import { useParams, useNavigate, Link } from 'react-router-dom';
import ServiceContext from '../Contexts/ServiceContext';
import ServiceForm from '../Components/ServiceForm';
import AuthContext from '../../contexts/AuthContext';

export default function EditService() {
  const [authState, authDispatch] = useContext(AuthContext);
  const [state, dispatch] = useContext(ServiceContext);
  const [showDelete, setShowDelete] = useState(false);
  const [authExpMessage, setAuthExpMessage] = useState(false);

  const navigate = useNavigate();

  let params = useParams();
  let url = "http://a464156a8a83a4311b2cde5c64247f4c-623028763.us-west-2.elb.amazonaws.com/api/services";
  let path = `/services/editPage/${params.serviceID}`

  const handleCloseDelete = () => {
    setShowDelete(false);

  }

  const handleShowDelete = () => {
    setShowDelete(true);
  }

  const deletePost = () => {
    const options = {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': authState.token
      }
    }
    fetch(`${url}/${params.serviceID}`, options)
    .then(response => {
      if (response.status === 401) {
        throw new Error();
      }
      if (response.ok) {
        dispatch({ type: 'deletePostFromManagePostsList', payload: params.serviceID })
      }
    })
      .then(() => {
        handleCloseDelete();
        navigate(-1);
      })
      .catch((error) => {
        // setShowDelete(false)
        setAuthExpMessage(true);
      });
  }

  const renderAuthExpMessage = () => {
    return (
    <Alert variant="danger" onClose={() => setAuthExpMessage(false)}>
        <Alert.Heading>Uh-oh, your session has timed out!</Alert.Heading>
        <p>
          You have been logged out of your account and your post has not been deleted.  Please log back in and try again.
        </p>
        <Link className='alert-link' to="/login">Login Here</Link>
      </Alert>
    )
  }

  return (
    <>
      
      <div className='background-newPost'>
        <ServiceForm path={path} handleShowDelete={handleShowDelete} serviceID={params.serviceID}></ServiceForm>
      </div>
      
      <Modal backdrop="static" show={showDelete} onHide={handleCloseDelete} className="verify-delete">
      {authExpMessage ? renderAuthExpMessage() : ""}
        <Modal.Header>
          <Modal.Title>⚠️ Are you sure?</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5>Deletions cannot be undone.</h5>
        </Modal.Body>
        <Modal.Footer>
          <Button disabled={authExpMessage} variant="success" onClick={deletePost} id="confirmDelete">Yes</Button>{' '}
          <Button disabled={authExpMessage} onClick={handleCloseDelete}>No</Button>
        </Modal.Footer>
      </Modal>
    </>
  )
};
