import { Modal, Button, Form, Stack, Card, Accordion, useAccordionButton, AccordionContext, Container, Row, Col } from 'react-bootstrap';
import React, { useContext, useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import ServiceDetail from './ServiceDetail';
import AuthContext from '../../contexts/AuthContext';
import '../ServiceApp.css';


function CustomToggle({ children, eventKey, callback }) {
    const { activeEventKey } = useContext(AccordionContext);
    const decoratedOnClick = useAccordionButton(eventKey, () =>
        callback && callback(eventKey),
    );

    const isCurrentEventKey = activeEventKey === eventKey;

    return (
        <button type="button" style={{ backgroundColor: isCurrentEventKey ? 'pink' : 'lavender' }} onClick={decoratedOnClick}>{children}</button>
    );
}

export default function ServiceCard({ service, handleFlagPost }) {
    const navigate = useNavigate();
    const [authState, authDispatch] = useContext(AuthContext);
    const [showDesc, setShowDesc] = useState(false)

    const navigateToEdit = () => {
        navigate(`/services/editPage/${service.id}`);
    }

    const handleDescExpand = () => {
        setShowDesc(!showDesc);
    }

    const renderButtonsProfile = () => {
        return <Button id="editProfileButton" variant="outline-info" onClick={navigateToEdit} size="sm">📝</Button>
    }

    const renderButtonsSearch = () => {
        return <>
            {service.username === authState.username ? <Button variant="outline-info" onClick={navigateToEdit} size="sm" id="mainPageEditButton">📝</Button> : ""}
            {service.violationFlagged ? <Button id="flaggedButton" size="sm" variant="outline-danger" onClick={handleFlagPost}>🚩</Button> : <Button id="unflaggedButton" size="sm" variant="outline-secondary" onClick={handleFlagPost}>&#9872;</Button>}
        </>
    }

    let inactiveLabel = service.activeStatus ? "" : " - DRAFT";

    return (
        <>
            <Card border="transparent" style={{ width: "28rem" }} className="cardBorder">
                <Card.Header as="h5" className={service.activeStatus ? "servicePostActive" : "servicePostInactive"}>
                    <Container fluid>
                        <Row>
                            <Col md="8" className="titleDescription">
                                {service.title}{inactiveLabel}
                            </Col>
                            <Col md="4" className='buttonSet'>
                                <Stack direction="horizontal" gap={1} className="userButtons">
                                    {window.location.pathname === '/profile' ? renderButtonsProfile() : renderButtonsSearch()}
                                </Stack>
                            </Col>
                        </Row>
                    </Container>
                </Card.Header>
                <Card.Body>
                    <Stack direction="horizontal" gap={1}>
                        <Card.Subtitle className="categoryDescription mt-0 mb-0">{service.category}</Card.Subtitle>
                        {service.lookingFor ? <Card.Text className="looking-for-service ms-auto">#InSearchOf</Card.Text> : <Card.Text className="ms-auto"></Card.Text>}
                    </Stack>
                    <Card.Text id="cardText" onClick={handleDescExpand} className={showDesc ? "serviceDescriptionx" : "serviceDescriptionx, text-truncate"}>{service.description}</Card.Text>
                    <Card.Footer className="mb-auto">
                        <Accordion defaultActiveKey="0">
                            <Card>
                                <CustomToggle eventKey="1">Show/Hide Details</CustomToggle>
                                <Accordion.Collapse eventKey="1">
                                    <Card.Body>
                                        <ServiceDetail key={service.id} service={service} />
                                    </Card.Body>
                                </Accordion.Collapse>
                            </Card>
                        </Accordion>
                    </Card.Footer>
                </Card.Body>
            </Card>
        </>
    )
};
