import React, { useEffect, useContext, useState } from 'react';
import Form from 'react-bootstrap/Form'
import ServiceCard from './ServiceCard';
import '../ServiceApp.css';
import AuthContext from '../../contexts/AuthContext';

export default function UsersInactiveServices() {
    const [authState, authDispatch] = useContext(AuthContext);
    const [activeServices, setActiveServices] = useState([]);

    let url = "http://a464156a8a83a4311b2cde5c64247f4c-623028763.us-west-2.elb.amazonaws.com/api/services"
    
    const loadPage = () => {
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': authState.token
            }
        }
        fetch(`${url}/user`, options).then((response) => {
            if (response.status === 204) {
                throw new Error();
            } else {
            return response.json();
            }
        }).then((data) => {
            let postsToManage = data.services.filter(service => service.activeStatus === false);
            setActiveServices(postsToManage);
        }).catch(error => {
            let postsToManage = [];
            setActiveServices(postsToManage);
        })
    }

    useEffect(() => {
        if (authState.token !== "") {
            loadPage();
        }
    }, [authState.token])


    const generatePosts = () => {
        if (activeServices.length === 0){
            return "No inactive post available"
        }
        let posts = activeServices.map((service) => {
                return <ServiceCard key={service.id} service={service} />
            }
        )
        return posts;
    }

    return (
        <Form>
            <Form.Group className="postsToManage">
                <div className="serviceListProfile">
                {generatePosts()}
                </div>
            </Form.Group>
        </Form>
    )
};
