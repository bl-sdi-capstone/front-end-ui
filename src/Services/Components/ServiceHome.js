import React, {useState} from 'react';
import ServiceList from './ServiceList';
import ServiceSearch from './ServiceSearch';
import { Container, Row, Col } from 'react-bootstrap';
import FilterContext from '../Contexts/FilterContext';
import '../ServiceApp.css';

export default function ServiceHome() {

    const [activeFilter, setActiveFilter] = useState(false);
    const [priceSelect, setPriceSelect] = useState(0);
    const [distanceSelect, setDistanceSelect] = useState(0);
    const [yoeSelect, setYoeSelect] = useState(0);
  

    return (
        <FilterContext.Provider value={[activeFilter, setActiveFilter, priceSelect, setPriceSelect, distanceSelect, setDistanceSelect, yoeSelect, setYoeSelect]}>
        <Container fluid className='serviceBody'>
            <Row>
                <Col md="2" className="search">
                    <ServiceSearch/>
                </Col>
                <Col md="10">
                    <ServiceList/>
                </Col>
            </Row>
        </Container>
        </FilterContext.Provider>
    )
};
