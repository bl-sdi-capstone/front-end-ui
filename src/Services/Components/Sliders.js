import React, {useContext} from 'react';
import FilterContext from '../Contexts/FilterContext';
import ServiceContext from '../Contexts/ServiceContext';
import { Form, Button, Stack} from "react-bootstrap";

export default function Sliders() {
 const [, setActiveFilter, priceSelect, setPriceSelect, distanceSelect, setDistanceSelect, yoeSelect, setYoeSelect] = useContext(FilterContext);
 const [state, dispatch] = useContext(ServiceContext);

let serviceListCopy = state.serviceList;

 const priceSlider = () => {
        let priceMin = 0;
        let priceMax = Math.max(
          ...serviceListCopy.map((service) => service?.priceValue ?? 0)
        );
    
        return (
          <Form.Group className="input-group mb-3">
            <Form.Label>Max Price:</Form.Label>
            <Form.Range
              min={priceMin}
              max={priceMax}
              value={priceSelect}
              onChange={(e) => setPriceSelect(e.target.value)}
            />
            <span className="input-group-text">$</span>
            <Form.Control
              value={priceSelect}
              onChange={(e) => setPriceSelect(e.target.value)}
            />
          </Form.Group>
        );
      };

      const yearsExperienceSlider = () => {
        let yoeMin = 0;
        let yoeMax = Math.max(
          ...serviceListCopy.map((service) => service?.experienceYears ?? 0)
        );
    
        return (
          <Form.Group className="input-group mb-3">
            <Form.Label>Minimum Years of Experience:</Form.Label>
            <Form.Range
              min={yoeMin}
              max={yoeMax}
              value={yoeSelect}
              onChange={(e) => setYoeSelect(e.target.value)}
            />
            <Form.Control
              value={yoeSelect}
              onChange={(e) => setYoeSelect(e.target.value)}
            />
            <span className="input-group-text">years</span>
          </Form.Group>
        );
      };

      const serviceRangeSlider = () => {
        let min = 0;
        let distanceMax = Math.max(
          ...serviceListCopy.map((service) => service?.serviceRange ?? 0)
        );
    
        return (
          <Form.Group className="input-group mb-3">
            <Form.Label>Max Distance:</Form.Label>
            <Form.Range
              min={min}
              max={distanceMax}
              value={distanceSelect}
              onChange={(e) => setDistanceSelect(e.target.value)}
            />
            <Form.Control
              value={distanceSelect}
              onChange={(e) => setDistanceSelect(e.target.value)}
            />
            <span className="input-group-text">miles</span>
          </Form.Group>
        );
      };
        
        const applyFilter = () => {
          let newList = [];

          let pattern = /\d+/g;
          let matchWhole = /^\d+$/g;

          if(parseInt(priceSelect) <= 0 || priceSelect === ""){
            setPriceSelect(0);
          } else if (!matchWhole.test(priceSelect)){
            setPriceSelect(priceSelect.match(pattern)[0]);
          };

          if(parseInt(distanceSelect) <= 0 || distanceSelect === ""){
            setDistanceSelect(0);
          } else if (distanceSelect !==0 && !matchWhole.test(distanceSelect)){
            setDistanceSelect(distanceSelect.match(pattern)[0]);
          }

          if(parseInt(yoeSelect) <= 0 || yoeSelect === ""){
            setYoeSelect(0);
          } else if(yoeSelect !==0 && !matchWhole.test(yoeSelect)){
            setYoeSelect(yoeSelect.match(pattern)[0]);
          }

          let price = (parseInt(priceSelect) <= 0 || priceSelect ==="") ? Infinity : priceSelect.match(pattern)[0];
          let distance = (parseInt(distanceSelect) <= 0 || distanceSelect ==="") ? Infinity : distanceSelect.match(pattern)[0];
          let yoe = (parseInt(yoeSelect) <= 0 || yoeSelect === "") ? 0 : yoeSelect.match(pattern)[0];

          if (price === Infinity && distance === Infinity && yoe === 0 ){
            return;
          }
    
          newList = serviceListCopy.filter((service) => {
            let sDistance = service?.serviceRange ?? 0;
            let sPrice = service?.priceValue ?? 0;
            let sYOE = service?.experienceYears ?? 0;
    
            return sDistance <= distance && sPrice <= price && sYOE >= yoe;
          });
    
          let categoryFreqSet = {};
          for (let i = 0; i < newList.length; i++) {
            let currentCategory = newList[i].category;
            categoryFreqSet[currentCategory] = categoryFreqSet[currentCategory]
              ? categoryFreqSet[currentCategory] + 1
              : 1;
          }
          let categoryArray = Object.keys(categoryFreqSet);
          let newCategoryCounts = categoryArray.map((category, i) => {
            return {
              id: i + 1,
              category: category,
              count: categoryFreqSet[category],
            };
          });
    
          newCategoryCounts.sort((a, b) =>
            a.category.toLowerCase() > b.category.toLowerCase() ? 1 : -1
          );
    
          setActiveFilter(true);
          dispatch({ type: "setCategories", payload: newCategoryCounts });
          dispatch({ type: "filterServiceList", payload: newList });
        }

        const disableApply = () => {

          return (serviceListCopy.length < 1 || (yoeSelect === 0 & priceSelect === 0 & distanceSelect === 0))
        }

        return (
              <Form.Group className="searchBarBottom">
                  <Form.Label className="fw-bold">Set Min/Max</Form.Label>
                  <div className="filter-buttons">
                    <fieldset disabled={serviceListCopy.length <= 1}>
                    {/* Apply Filter button below also disables with same check */}
                        {priceSlider()}
                        {serviceRangeSlider()}
                        {yearsExperienceSlider()}
                    </fieldset>
                    <Stack direction="horizontal" gap={1}>
                        <Button variant="primary" sx={{ backgroundColor: "#26c6da", margin: "10px" }} onClick={applyFilter} disabled={disableApply()} id="applyFilterButton">Apply Min/Max </Button>
                    </Stack>                  
                  </div>
              </Form.Group>
          );
};
