import React, { useContext, useEffect, useState } from "react";
import ServiceContext from "../Contexts/ServiceContext";
import '../ServiceApp.css';
import AuthContext from '../../contexts/AuthContext';
import ServiceCard from "./ServiceCard";


export default function Service({ service }) {
  const [state, dispatch] = useContext(ServiceContext);
  const [authState, authDispatch] = useContext(AuthContext);
  const [flagged, setFlagged] = useState(service.violationFlagged);

  let flaggedUrl = "http://a464156a8a83a4311b2cde5c64247f4c-623028763.us-west-2.elb.amazonaws.com/api/flagged"

  const handleFlagPost = (e) => {
    let flaggedValue = !flagged;
    setFlagged(flaggedValue);
  }

  useEffect(() => {
    flagPost();
  }, [flagged])

  const flagPost = () => {
    if (flagged !== service.violationFlagged) {
      const postToFlag = {
        violationFlagged: flagged
      }
      
      const options = {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': authState.token
        },
        body: JSON.stringify(postToFlag)
      };
      fetch(`${flaggedUrl}/${service.id}`, options).then(response => {
        if (response.ok) {
          let allServices = [...state.serviceList];
          allServices.map(svc => {
            if (svc.id === service.id) {
              svc.violationFlagged = !svc.violationFlagged;
            }
            dispatch({ type: "flaggedService", payload: allServices })
          })
        }
      })
    }
  }

  return (
      <ServiceCard service={service} handleFlagPost={handleFlagPost} ></ServiceCard>
  )
};
