import Button from 'react-bootstrap/Button';
import React, { useContext } from "react";
import ServiceContext from '../Contexts/ServiceContext';
import FilterContext from '../Contexts/FilterContext';
import '../ServiceApp.css';
import { Container, Form } from 'react-bootstrap';


export default function ServiceCategoryItem({ category }) {
    const [, setActiveFilter] = useContext(FilterContext);


    const [state, dispatch] = useContext(ServiceContext)
    let baseUrl = "http://a464156a8a83a4311b2cde5c64247f4c-623028763.us-west-2.elb.amazonaws.com/api/services";

    const getResults = () => {

        setActiveFilter(true);

            let serviceListCopy = state.serviceList;
            let filteredList = serviceListCopy.filter(service => service.category === category.category);
            let categoryCounts = state.categoryCounts;
            let filteredCategories = categoryCounts.filter(categoryObj => categoryObj.category === category.category);
            dispatch({type: 'setCategories', payload: filteredCategories});
            dispatch({type: 'addServiceList', payload: filteredList});
    }

    return (
        <Container className='radioButtonGroup'>
            <Button className="cat-link" variant="text" onClick={getResults}>{category.category} ({category.count})</Button>
        </Container>
    )

};
