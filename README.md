# [Yard Sale Item, Service and User](https://gitlab.com/stfa-ase-0322/capstone)


AWS UI : (http://a9bbf8e1a16fe402ebe70d71178aed14-591933515.us-west-2.elb.amazonaws.com/)

AWS API : (http://aae7c78e5e3954b3e8042f4d9023d58f-346942550.us-west-2.elb.amazonaws.com/api/users)

Swagger : (http://aae7c78e5e3954b3e8042f4d9023d58f-346942550.us-west-2.elb.amazonaws.com/swagger-ui.html#/)

Microservice repo(s) :
 - FindByUsername (UserRepository)
 - findById (UserImageRepository)

 Scott Sauer : Setting up Tests/ automated test cases, initial Front End development

 Matt Wood : Initial Back End development, CSS design work, profile picture integration back end

 Chris Webster : Initial Front End development, profile picture integration front end

 Chris Knoer : Ininitial Back end development, profile picture integration back end and front end




## Documentation 
- More to come

## Available Scripts
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

All defaults apply for starting application, but primarly in the project directory, you can run:

### `npm start`
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.
