# build environment
FROM node:14.17.1 as build
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
# copy only the package files to install dependencies
# allows this layer to be cached as src/ changes frequently
# leads to faster builds
COPY package.json package-lock.json ./
RUN npm ci --silent && \
    npm install react-scripts@3.4.1 -g --silent
COPY public/ public/
COPY src/ src/
# COPY additional files before build if they are required
RUN npm run build

# run environment
FROM nginx:stable-alpine
COPY --from=build /app/build /usr/share/nginx/html
# replace the default ngix config with the custom one
COPY nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]